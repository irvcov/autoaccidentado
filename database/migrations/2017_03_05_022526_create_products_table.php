<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('model');
            $table->integer('category_id');
            $table->foreign('category_id')->references('id')->on('categories');
            $table->string('details');
            $table->string('brand');
            $table->string('measure');
            $table->double('price')->default(1000);
            $table->string('image');
            $table->string('thumbnail_1');
            $table->string('thumbnail_2');
            $table->string('thumbnail_3');
            $table->string('thumbnail_4');
            $table->string('thumbnail_5');
            $table->string('setup_conditions',700);
            $table->string('specs_file');
            $table->boolean('promotion');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
