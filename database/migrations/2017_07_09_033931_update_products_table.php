<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('products', function (Blueprint $table) {

            $table->string('state');
            $table->string('subsidiary');
            $table->string('year');
            $table->string('transmition');
            $table->integer('kms');
            $table->string('exterior');
            $table->string('keys');
            $table->string('verification');
            $table->double('bill')->default(1000);
            $table->string('type');
            $table->string('combustible');
            $table->string('interior');
            $table->string('doors');
            $table->string('tenure');
            $table->double('bill2')->default(1000);
            $table->string('observations',700);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        $table->dropColumn('setup_conditions');
        $table->dropColumn('measure');
        $table->dropColumn('specs_file');
        $table->dropColumn('details');
    }
}
