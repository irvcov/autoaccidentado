<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductTable3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('products', function (Blueprint $table) {
        $table->dropColumn('setup_conditions');
        $table->dropColumn('measure');
        $table->dropColumn('specs_file');
        $table->dropColumn('details');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('products', function (Blueprint $table) {
        $table->dropColumn('setup_conditions');
        $table->dropColumn('measure');
        $table->dropColumn('specs_file');
        $table->dropColumn('details');
        });
    }
}
