<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','SlideController@show');

/*Route::get('contacto', function () {
    return view('contacto');
});*/

Route::get('como-comprar-un-auto', function () {
    return view('como-comprar-un-auto');
});


//Route::get('seminuevos','ProductsController@promotions');
//Route::get('accidentados','ProductsController@promotions');
//Route::get('recuperados','ProductsController@promotions');

Route::get('products/{category}','ProductsController@products');


Route::get('detalle/add/{id}','DetalleController@add');
//Route::get('detalle/add2/{id}','DetalleController@add2');

Route::get('index','SlideController@show');
Route::get('contacto','SlideController@contacto');

/*
Route::get('eventos', function () {
    return view('eventos');
});

Route::get('formulario', function () {
    return view('formulario');
});

Route::get('detalle2', function () {
    return view('detalle');
});

Route::get('nosotros', function () {
    return view('nosotros');
});
*/

Route::group([
    'prefix' => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['admin'],
    'namespace' => 'Admin'
], function() {
    // your CRUD resources and other admin routes here
    Route::get('dashboard', 'DashboardController@index');
    CRUD::resource('category', 'CategoryCrudController');
    CRUD::resource('parentcategory', 'ParentCategoryCrudController');
    CRUD::resource('product', 'ProductCrudController');
    Route::get('dash', 'DashboardController@index');
	CRUD::resource('slide', 'SlideCrudController');
    //CRUD::resource('detalle', 'SlideCrudController');
});

