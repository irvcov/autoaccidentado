<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ProductRequest as StoreRequest;
use App\Http\Requests\ProductRequest as UpdateRequest;

class ProductCrudController extends CrudController
{

    public function setUp()
    {

        /*
		|--------------------------------------------------------------------------
		| BASIC CRUD INFORMATION
		|--------------------------------------------------------------------------
		*/
        $this->crud->setModel("App\Models\Product");
        $this->crud->setRoute("admin/product");
        $this->crud->setEntityNameStrings('producto', 'productos');

        /*
		|--------------------------------------------------------------------------
		| BASIC CRUD INFORMATION
		|--------------------------------------------------------------------------
		*/

        $this->crud->setFromDb();

        $this->crud->removeField("model");
        $this->crud->removeField("year");
        $this->crud->removeField("state");
        $this->crud->removeField("subsidiary");
        $this->crud->removeField("transmition");
        $this->crud->removeField("kms");
        $this->crud->removeField("exterior");
        $this->crud->removeField("llaves");
        $this->crud->removeField("verification");
        $this->crud->removeField("combustible");
        $this->crud->removeField("interior");
        $this->crud->removeField("doors");
        $this->crud->removeField("tenure");
        $this->crud->removeField("brand");
        $this->crud->removeField("type");
        $this->crud->removeField("engine");
        $this->crud->removeField("observations");
        $this->crud->removeField("price");
        $this->crud->removeField("bill");
        $this->crud->removeField("image");
        $this->crud->removeField("thumbnail_1");
        $this->crud->removeField("thumbnail_2");
        $this->crud->removeField("thumbnail_3");
        $this->crud->removeField("thumbnail_4");
        $this->crud->removeField("thumbnail_5");

        $this->crud->addField([  // Select
           'label' => "Nombre del producto",
           'type' => 'text',
           'name' => 'name'
        ]);

        $this->crud->addField([  // Select
           'label' => "Modelo",
           'type' => 'text',
           'name' => 'model'
        ]);

        $this->crud->addField([  // Select
           'label' => "Año",
           'type' => 'text',
           'name' => 'year'
        ]);

        $this->crud->addField([  // Select
           'label' => "Marca",
           'type' => 'text',
           'name' => 'brand'
        ]);

        $this->crud->addField([  // Select
           'label' => "Tipo de Auto",
           'type' => 'text',
           'name' => 'type'
        ]);

        $this->crud->addField([  // Select
           'label' => "motor",
           'type' => 'text',
           'name' => 'engine'
        ]);

        $this->crud->addField([  // Select
           'label' => "Estado",
           'type' => 'text',
           'name' => 'state'
        ]);

        $this->crud->addField([  // Select
           'label' => "Sucursal",
           'type' => 'text',
           'name' => 'subsidiary'
        ]);
        $this->crud->addField([  // Select
           'label' => "Transmision",
           'type' => 'text',
           'name' => 'transmition'
        ]);
        $this->crud->addField([  // Select
           'label' => "kms",
           'type' => 'number',
           'name' => 'kms'
        ]);
        $this->crud->addField([  // Select
           'label' => "Exterior",
           'type' => 'text',
           'name' => 'exterior'
        ]);
        $this->crud->addField([  // Select
           'label' => "Llaves",
           'type' => 'text',
           'name' => 'llaves'
        ]);
        $this->crud->addField([  // Select
           'label' => "Verificacion",
           'type' => 'text',
           'name' => 'verification'
        ]);
        
        $this->crud->addField([  // Select
           'label' => "Combustible",
           'type' => 'text',
           'name' => 'combustible'
        ]);
        $this->crud->addField([  // Select
           'label' => "Interior",
           'type' => 'text',
           'name' => 'interior'
        ]);

        $this->crud->addField([  // Select
           'label' => "Puertas",
           'type' => 'text',
           'name' => 'doors'
        ]);

        $this->crud->addField([  // Select
           'label' => "Tenencia",
           'type' => 'text',
           'name' => 'tenure'
        ]);

        $this->crud->addField([  // Select
           'label' => "Observaciones",
           'type' => 'textarea',
           'name' => 'observations'
        ]);

        $this->crud->addField([  // Select
           'label' => "Precio",
           'type' => 'number',
            // optionals
            'attributes' => ["step" => "any"], // allow decimals
            'prefix' => "$",
            //'suffix' => ".00",
           'name' => 'price'
        ]);

        $this->crud->addField([  // Select
           'label' => "Factura",
           'type' => 'text',
            // optionals
            //'attributes' => ["step" => "any"], // allow decimals
            //'prefix' => "$",
            //'suffix' => ".00",
           'name' => 'bill'
        ]);

        /*$this->crud->addField([  // Select
           'label' => "Factura 2",
           'type' => 'number',
            // optionals
            'attributes' => ["step" => "any"], // allow decimals
            'prefix' => "$",
            //'suffix' => ".00",
           'name' => 'bill2'
        ]);*/

        $this->crud->removeField("category_id");
        $this->crud->addField([  // Select
           'label' => "Categoría",
           'type' => 'select',
           'name' => 'category_id', // the db column for the foreign key
           'entity' => 'category', // the method that defines the relationship in your Model
           'attribute' => 'name', // foreign key attribute that is shown to user
           'model' => "App\Models\Category" // foreign key model
        ]);

        $this->crud->removeField('StateProduct_id');
        $this->crud->addField([  // Select
           'label' => "Estatus del Producto",
           'type' => 'select',
           'name' => 'StateProduct_id', // the db column for the foreign key
           'entity' => 'stateproduct', // the method that defines the relationship in your Model
           'attribute' => 'name', // foreign key attribute that is shown to user
           'model' => "App\Models\StateProduct" // foreign key model
        ]);

        $this->crud->addField([ // image
            'label' => "Imagen Principal",
            'name' => "image",
            //'filename' => null,
            'type' => 'image',
            'upload' => true,
            'crop' => false, // set to true to allow cropping, false to disable
            'aspect_ratio' => 1, // ommit or set to 0 to allow any aspect ratio
            //'src' => 'null'
        ]);
        
        /*$this->crud->addField([ // image
            'label' => "Archivo de especificaciones",
            'name' => "specs_file",
            'type' => 'upload',
            'upload' => true
        ]);*/

        $this->crud->addField([ // image
            'label' => "Imagen 1",
            'name' => "thumbnail_1",
            'type' => 'image',
            'upload' => true,
            'crop' => false, // set to true to allow cropping, false to disable
            'aspect_ratio' => 1, // ommit or set to 0 to allow any aspect ratio
            //'src' => 'setThumbnail1Attribute'
        ]);

        $this->crud->addField([ // image
            'label' => "Imagen 2",
            'name' => "thumbnail_2",
            'type' => 'image',
            'upload' => true,
            'crop' => false, // set to true to allow cropping, false to disable
            'aspect_ratio' => 1, // ommit or set to 0 to allow any aspect ratio
            //'src' => 'setThumbnail2Attribute'
        ]);

        $this->crud->addField([ // image
            'label' => "Imagen 3",
            'name' => "thumbnail_3",
            'type' => 'image',
            'upload' => true,
            'crop' => false, // set to true to allow cropping, false to disable
            'aspect_ratio' => 1, // ommit or set to 0 to allow any aspect ratio
            //'src' => 'setThumbnail3Attribute'
        ]);

       /* $this->crud->addField([ // image
            'label' => "Imagen 4",
            'name' => "thumbnail_4",
            'type' => 'image',
            'upload' => true,
            'crop' => false, // set to true to allow cropping, false to disable
            'aspect_ratio' => 0, // ommit or set to 0 to allow any aspect ratio
            //'src' => 'setThumbnail4Attribute'
        ]);

        $this->crud->addField([ // image
            'label' => "Imagen 5",
            'name' => "thumbnail_5",
            'type' => 'image',
            'upload' => true,
            'crop' => false, // set to true to allow cropping, false to disable
            'aspect_ratio' => 0, // ommit or set to 0 to allow any aspect ratio
            //'src' => 'setThumbnail5Attribute'
        ]); */



        // ------ CRUD FIELDS
        // $this->crud->addField($options, 'update/create/both');
        // $this->crud->addFields($array_of_arrays, 'update/create/both');
        // $this->crud->removeField('name', 'update/create/both');
        // $this->crud->removeFields($array_of_names, 'update/create/both');

        // ------ CRUD COLUMNS
        $this->crud->removeColumn("name");
        $this->crud->removeColumn("model");
        $this->crud->removeColumn("brand");
        $this->crud->removeColumn("year");
        $this->crud->removeColumn("kms");
        $this->crud->removeColumn("type");
        $this->crud->removeColumn("price");

        $this->crud->addColumn([  // Select
           'label' => "Nombre",
           'type' => 'text',
           'name' => 'name'
        ]);
        $this->crud->addColumn([  // Select
           'label' => "Modelo",
           'type' => 'text',
           'name' => 'model'
        ]);
        $this->crud->addColumn([  // Select
           'label' => "Marca",
           'type' => 'text',
           'name' => 'brand'
        ]);
        $this->crud->addColumn([  // Select
           'label' => "Año",
           'type' => 'number',
           'name' => 'year'
        ]);
        $this->crud->addColumn([  // Select
           'label' => "Kilometros",
           'type' => 'text',
           'name' => 'kms'
        ]);
        $this->crud->addColumn([  // Select
           'label' => "Tipo",
           'type' => 'text',
           'name' => 'type'
        ]);
        $this->crud->addColumn([  // Select
           'label' => "Precio",
           'type' => 'text',
           'name' => 'price'
        ]);

        $this->crud->removeColumn("StateProduct_id");
        /*$this->crud->addColumn([  // Select
           'label' => "Estatus del Producto",
           'type' => 'select',
           'name' => 'StateProduct_id', // the db column for the foreign key
           'entity' => 'stateproduct', // the method that defines the relationship in your Model
           'attribute' => 'name', // foreign key attribute that is shown to user
           'model' => "App\Models\StateProduct" // foreign key model
        ]);*/


        
        $this->crud->removeColumn("category_id");
        $this->crud->addColumn([  // Select
           'label' => "Categoría",
           'type' => 'select',
           'name' => 'category_id', // the db column for the foreign key
           'entity' => 'category', // the method that defines the relationship in your Model
           'attribute' => 'name', // foreign key attribute that is shown to user
           'model' => "App\Models\Category" // foreign key model
        ]);

        $this->crud->removeColumn("image");
        $this->crud->removeColumn("state");
        $this->crud->removeColumn("subsidiary");
        $this->crud->removeColumn("exterior");
        $this->crud->removeColumn("llaves");
        $this->crud->removeColumn("verification");
        $this->crud->removeColumn("bill");
        $this->crud->removeColumn("interior");
        $this->crud->removeColumn("doors");
        $this->crud->removeColumn("observations");
        $this->crud->removeColumn("tenure");
        $this->crud->removeColumn("combustible");
        $this->crud->removeColumn("engine");
        $this->crud->removeColumn("transmition");
        $this->crud->removeColumn("thumbnail_1");
        $this->crud->removeColumn("thumbnail_2");
        $this->crud->removeColumn("thumbnail_3");
        $this->crud->removeColumn("thumbnail_4");
        $this->crud->removeColumn("thumbnail_5");
        //$this->crud->removeColumn("specs_file");

        $this->crud->addColumn([ // image
            'label' => "Imagen",
            'name' => "image",
            'type' => 'image',
            'attributes' => [
                'style'=>"width: 60px;",
                'style'=>"height: 60px;"
            ],
        ]);



        // $this->crud->addColumn(); // add a single column, at the end of the stack
        // $this->crud->addColumns(); // add multiple columns, at the end of the stack
        // $this->crud->removeColumn('column_name'); // remove a column from the stack
        // $this->crud->removeColumns(['column_name_1', 'column_name_2']); // remove an array of columns from the stack
        // $this->crud->setColumnDetails('column_name', ['attribute' => 'value']); // adjusts the properties of the passed in column (by name)
        // $this->crud->setColumnsDetails(['column_1', 'column_2'], ['attribute' => 'value']);

        // ------ CRUD BUTTONS
        // possible positions: 'beginning' and 'end'; defaults to 'beginning' for the 'line' stack, 'end' for the others;
        // $this->crud->addButton($stack, $name, $type, $content, $position); // add a button; possible types are: view, model_function
        // $this->crud->addButtonFromModelFunction($stack, $name, $model_function_name, $position); // add a button whose HTML is returned by a method in the CRUD model
        // $this->crud->addButtonFromView($stack, $name, $view, $position); // add a button whose HTML is in a view placed at resources\views\vendor\backpack\crud\buttons
        // $this->crud->removeButton($name);
        // $this->crud->removeButtonFromStack($name, $stack);

        // ------ CRUD ACCESS
        // $this->crud->allowAccess(['list', 'create', 'update', 'reorder', 'delete']);
        // $this->crud->denyAccess(['list', 'create', 'update', 'reorder', 'delete']);

        // ------ CRUD REORDER
        // $this->crud->enableReorder('label_name', MAX_TREE_LEVEL);
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('reorder');

        // ------ CRUD DETAILS ROW
        // $this->crud->enableDetailsRow();
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('details_row');
        // NOTE: you also need to do overwrite the showDetailsRow($id) method in your EntityCrudController to show whatever you'd like in the details row OR overwrite the views/backpack/crud/details_row.blade.php

        // ------ REVISIONS
        // You also need to use \Venturecraft\Revisionable\RevisionableTrait;
        // Please check out: https://laravel-backpack.readme.io/docs/crud#revisions
        // $this->crud->allowAccess('revisions');

        // ------ AJAX TABLE VIEW
        // Please note the drawbacks of this though:
        // - 1-n and n-n columns are not searchable
        // - date and datetime columns won't be sortable anymore
        //$this->crud->enableAjaxTable();

        // ------ DATATABLE EXPORT BUTTONS
        // Show export to PDF, CSV, XLS and Print buttons on the table view.
        // Does not work well with AJAX datatables.
        // $this->crud->enableExportButtons();

        // ------ ADVANCED QUERIES
        // $this->crud->addClause('active');
        // $this->crud->addClause('type', 'car');
        // $this->crud->addClause('where', 'name', '==', 'car');
        // $this->crud->addClause('whereName', 'car');
        // $this->crud->addClause('whereHas', 'posts', function($query) {
        //     $query->activePosts();
        // });
        // $this->crud->with(); // eager load relationships
        // $this->crud->orderBy();
        // $this->crud->groupBy();
        // $this->crud->limit();

        $this->crud->removeColumn("photos");
        $this->crud->removeField("photos");
        $this->crud->addField([   // Upload
              'name' => 'photos',
              'label' => 'Mas Fotos',
              'type' => 'upload_multiple',
              'upload' => true,
              'disk' => 'uploads' // if you store files in the /public folder, please ommit this; if you store them in /storage or S3, please specify it;
        ]);
        
        $this->crud->setDefaultPageLength(8);
    }

	public function store(StoreRequest $request)
	{
		// your additional operations before save here
        $redirect_location = parent::storeCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
	}

	public function update(UpdateRequest $request)
	{
		// your additional operations before save here
        $redirect_location = parent::updateCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
	}
}
