<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers;


class DashboardController extends \App\Http\Controllers\Controller
{
    public function index(Request $request)
    {
            $products = DB::table('products')->count();
            $categories = DB::table('categories')->count();
			$slides = DB::table('slides')->count();
        return view('dashboard2' , ['products' => $products, 'categories' => $categories, 'slides' => $slides]);
    }

}
