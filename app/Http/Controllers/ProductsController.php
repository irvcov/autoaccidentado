<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class ProductsController extends Controller
{


    public function products($category)
    {
        $stateproduct = DB::table('StateProduct')->get();  
		$categories = DB::table('categories')->get();   //->select('categories.image')
        $products = DB::table('products')
        ->select('*')
        ->where('category_id',"=",$category)
        ->orderBy('name', 'asc')
        ->get();

        return view('seminuevos' , ['products' => $products, 'categories' => $categories, 'stateproduct' => $stateproduct]);
        //return view('seminuevos' , ['products' => $products, 'promotion' => false]);
    }

    public function promotions()
    {
		//$categories = DB::table('categories')->get();   //->select('categories.image')
        $products = DB::table('products')
        ->select('*')
        ->where('promotion',"=",true)
        ->orderBy('name', 'asc')
        ->get();
        //return view('catalogo2' , ['products' => $products, 'categories' => $categories]);
        return view('seminuevos' , ['products' => $products, 'promotion' => true]);
    }

    

}
