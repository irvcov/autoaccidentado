<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Cookie;
use Log;

class SlideController extends Controller
{

    public function show(Request $request){
    	//$images = DB::table('slides')->get();
		$images = DB::table('slides')->select('slides.image')->get();
        $categories = DB::table('categories')->get(); 
    	
    	if($images == null){
    		$images = [];
    	}
    	
    	/*Log::info('This is some useful information.');
    	shell_exec('echo "hello world" 1>&2');
    	error_log('Some message here.');*/

        return view('index' , ['images' => $images, 'categories' => $categories]);
    }


    public function contacto(Request $request){
        $categories = DB::table('categories')->get(); 
        return view('contacto' , ['categories' => $categories]);
    }
    
}
