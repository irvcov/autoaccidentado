<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Cookie;


class CartController extends Controller
{
    public function add(Request $request,$id)
    {
    	$total = 0;
    	$grandTotal = 0;
    	$deliveryCharge = 125;
    	$quantity = 1;
    	if($request->has("q")){
    		$tmpQuantity = $request->input("q");
    		//quantity has to be at least 1 
    		if(is_numeric($tmpQuantity) && $tmpQuantity > 0){
    			$quantity = $tmpQuantity;
    			// if($quantity > 10)
    			// {
    			// 	$quantity = 10;
    			// }
    		}
    	}
    	$products = $request->cookie('order');
    	if($products == null){
    		$products = [];
    	}
    	else{
    		$products = json_decode($products);
    	}
    	$newProduct = true;
    	foreach ($products as $key => $product) {
        	if($product->id == $id){
        		    $newProduct = false;
        		    $products[$key]->quantity = $quantity;
        	}

        	$currentTotal = $products[$key]->quantity * $product->price;
        	$products[$key]->total = $currentTotal;
        	$total += $currentTotal;
        }
        if($newProduct){
        	$product = DB::table('products')
	        ->leftJoin('categories', 'products.category_id', '=', 'categories.id')
	        ->select('categories.name as category_name','products.*')
	        ->where('products.id',"=",$id)
	        ->first();
	        if($product != null){
	        	$product->quantity = $quantity;
	        	$product->total = $product->quantity * $product->price;
        		$total += $product->total;
	        	array_push($products, $product);
	        }
	        
        }
        Cookie::queue('order', json_encode($products) , 44640);
        $grandTotal = $total + $deliveryCharge;
        return view('carrito' , ['products' => $products, 'grandTotal' => $grandTotal,
        						 'total' => $total,'deliveryCharge' => $deliveryCharge]);
    }

    public function remove(Request $request,$id){
    	$total = 0;
        $grandTotal = 0;
        $deliveryCharge = 125;

        $products = $request->cookie('order');
        if($products == null){
            $products = [];
        }
        else{
            $products = json_decode($products);
        }
        $arrayIndex = -1;
        foreach ($products as $key => $product) {
            if($product->id == $id){
                    $arrayIndex = $key;
            }
            else{
                $total += $product->total;    
            }
            
        }
        if($arrayIndex != -1){
            array_splice($products, $arrayIndex , 1);
        }
        Cookie::queue('order', json_encode($products) , 44640);
        $grandTotal = $total + $deliveryCharge;
        return view('carrito' , ['products' => $products, 'grandTotal' => $grandTotal,
                                 'total' => $total,'deliveryCharge' => $deliveryCharge]);
    }

    public function show(Request $request){
    	$products = $request->cookie('order');
    	$total = 0;
    	$grandTotal = 0;
    	$deliveryCharge = 125;

    	if($products == null){
    		$products = [];
    	}
    	else{
    		$products = json_decode($products);
    	}

    	foreach ($products as $key => $product) {
        	$total += $product->total;
        }

        $grandTotal = $total + $deliveryCharge;
        return view('carrito' , ['products' => $products, 'grandTotal' => $grandTotal,
        						 'total' => $total,'deliveryCharge' => $deliveryCharge]);
    }
}
