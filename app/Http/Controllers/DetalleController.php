<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class DetalleController extends Controller
{
    /*public function add($id)
    {
       $product = $id;
       return view('detalle' , ['product' => $product]);
    }

    /*public function show(Request $request){
    	return view('detalle' , ['products' => $products, 'categories' => $categories]);
    }*/

    public function add($id)
    {
        $categories = DB::table('categories')->get();      
        $product = DB::table('products')
	        ->select('*')
	        ->where('products.id',"=",$id)
	        ->first();
       return view('detalle-producto' , ['product' => $product, 'categories' => $categories]);
    }

}