<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

use Image;

class Product extends Model
{
    use CrudTrait;

     /*
	|--------------------------------------------------------------------------
	| GLOBAL VARIABLES
	|--------------------------------------------------------------------------
	*/

    protected $table = 'products';
    //protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
     protected $fillable = ['id','name','model','category_id','brand','price','image','thumbnail_1','thumbnail_2','thumbnail_3', /*'thumbnail_4', 'thumbnail_5', */'state','subsidiary','year','transmition','kms','exterior','llaves','verification','bill','type','combustible','interior','doors','tenure','photos','observations','StateProduct_id','engine'];
    // protected $hidden = [];
    // protected $dates = [];

    protected $casts = [
        'photos' => 'array'
    ];

    /*
	|--------------------------------------------------------------------------
	| FUNCTIONS
	|--------------------------------------------------------------------------
	*/

    /*
	|--------------------------------------------------------------------------
	| RELATIONS
	|--------------------------------------------------------------------------
	*/
    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    public function parentcategory()
    {
        return $this->belongsTo('App\Models\ParentCategory');
    }

    public function stateproduct()
    {
        return $this->belongsTo('App\Models\StateProduct');
    }

    /*
	|--------------------------------------------------------------------------
	| SCOPES
	|--------------------------------------------------------------------------
	*/

    /*
	|--------------------------------------------------------------------------
	| ACCESORS
	|--------------------------------------------------------------------------
	*/

    /*
	|--------------------------------------------------------------------------
	| MUTATORS
	|--------------------------------------------------------------------------
	*/
	
	/*public function setImageAttribute($value)
    {
        $attribute_name = "image";
        $disk = "public";
        $destination_path = "uploads/products";

        $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path);
    }*/

    public function setPhotosAttribute($value)
    {
        $attribute_name = "photos";
        $disk = "public_uploads";
        $destination_path = "uploads/products";

        $this->uploadMultipleFilesToDisk($value, $attribute_name, $disk, $destination_path);
    }
	
	public function setImageAttribute($value)
    {
        $attribute_name = "image";
        $disk = "public_uploads";
        $destination_path = "uploads/products";

        // if the image was erased
        if ($value==null) {
            // delete the image from disk
            \Storage::disk($disk)->delete($this->image);

            // set null in the database column
            $this->attributes[$attribute_name] = null;
        }

        // if a base64 was sent, store it in the db
        if (starts_with($value, 'data:image'))
        {
            // 0. Make the image
            $image = \Image::make($value);
            // 1. Generate a filename.
            $filename = md5($value.time()).'.jpg';
            // 2. Store the image on disk.
            \Storage::disk($disk)->put($destination_path.'/'.$filename, $image->stream());
            // 3. Save the path to the database
            $this->attributes[$attribute_name] = $destination_path.'/'.$filename;
        }
        
        //$this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path);
        
        /*$image=$value;
        $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
        $img = Image::make($image->getRealPath());
    
        $destinationPath = "uploads/products";
        //$destinationPath = public_path('/uploads/products');
        $img->resize(750, 450, function ($constraint) {
            $constraint->aspectRatio();
        })->save($destinationPath.'/'.$input['imagename']);
        
        $this->attributes['image'] =   strtolower($input['imagename']);  */

    }

    public function setThumbnail1Attribute($value)
    {
        $attribute_name = "thumbnail_1";
        $disk = "public_uploads";
        $destination_path = "uploads/products";

        // if the image was erased
        if ($value==null) {
            // delete the image from disk
            \Storage::disk($disk)->delete($this->image);

            // set null in the database column
            $this->attributes[$attribute_name] = null;
        }

        // if a base64 was sent, store it in the db
        if (starts_with($value, 'data:image'))
        {
            // 0. Make the image
            $image = \Image::make($value);
            // 1. Generate a filename.
            $filename = md5($value.time()).'.jpg';
            // 2. Store the image on disk.
            \Storage::disk($disk)->put($destination_path.'/'.$filename, $image->stream());
            // 3. Save the path to the database
            $this->attributes[$attribute_name] = $destination_path.'/'.$filename;
        }
    }

    public function setThumbnail2Attribute($value)
    {
        $attribute_name = "thumbnail_2";
        $disk = "public_uploads";
        $destination_path = "uploads/products";

        // if the image was erased
        if ($value==null) {
            // delete the image from disk
            \Storage::disk($disk)->delete($this->image);

            // set null in the database column
            $this->attributes[$attribute_name] = null;
        }

        // if a base64 was sent, store it in the db
        if (starts_with($value, 'data:image'))
        {
            // 0. Make the image
            $image = \Image::make($value);
            // 1. Generate a filename.
            $filename = md5($value.time()).'.jpg';
            // 2. Store the image on disk.
            \Storage::disk($disk)->put($destination_path.'/'.$filename, $image->stream());
            // 3. Save the path to the database
            $this->attributes[$attribute_name] = $destination_path.'/'.$filename;
        }
    }

    public function setThumbnail3Attribute($value)
    {
        //$value = base64_decode($value);
        
        $attribute_name = "thumbnail_3";
        $disk = "public_uploads";
        $destination_path = "uploads/products";

        // if the image was erased
        if ($value==null) {
            // delete the image from disk
            \Storage::disk($disk)->delete($this->image);

            // set null in the database column
            $this->attributes[$attribute_name] = null;
        }

        // if a base64 was sent, store it in the db
        if (starts_with($value, 'data:image'))
        {
            // 0. Make the image
            $image = \Image::make($value);
            // 1. Generate a filename.
            $filename = md5($value.time()).'.jpg';
            // 2. Store the image on disk.
            \Storage::disk($disk)->put($destination_path.'/'.$filename, $image->stream());
            // 3. Save the path to the database
            $this->attributes[$attribute_name] = $destination_path.'/'.$filename;
        }
    }

    public function setThumbnail4Attribute($value)
    {
        $attribute_name = "thumbnail_4";
        $disk = "public_uploads";
        //$disk = "public_folder";
        $destination_path = "uploads/products";

        // if the image was erased
        if ($value==null) {
            // delete the image from disk
            \Storage::disk($disk)->delete($this->image);

            // set null in the database column
            $this->attributes[$attribute_name] = null;
        }

        // if a base64 was sent, store it in the db
        if (starts_with($value, 'data:image'))
        {
            // 0. Make the image
            $image = \Image::make($value);
            // 1. Generate a filename.
            $filename = md5($value.time()).'.jpg';
            // 2. Store the image on disk.
            \Storage::disk($disk)->put($destination_path.'/'.$filename, $image->stream());
            // 3. Save the path to the database
            $this->attributes[$attribute_name] = $destination_path.'/'.$filename;
        }
    }

    public function setThumbnail5Attribute($value)
    {
        $attribute_name = "thumbnail_5";
        $disk = "public_uploads";
        $destination_path = "uploads/products";

        // if the image was erased
        if ($value==null) {
            // delete the image from disk
            \Storage::disk($disk)->delete($this->image);

            // set null in the database column
            $this->attributes[$attribute_name] = null;
        }

        // if a base64 was sent, store it in the db
        if (starts_with($value, 'data:image'))
        {
            // 0. Make the image
            $image = \Image::make($value);
            // 1. Generate a filename.
            $filename = md5($value.time()).'.jpg';
            // 2. Store the image on disk.
            \Storage::disk($disk)->put($destination_path.'/'.$filename, $image->stream());
            // 3. Save the path to the database
            $this->attributes[$attribute_name] = $destination_path.'/'.$filename;
        }
    }

}
