<!DOCTYPE html>
<html class="html" lang="es-ES">
 <head>

  <script type="text/javascript">
   if(typeof Muse == "undefined") window.Muse = {}; window.Muse.assets = {"required":["jquery-1.8.3.min.js", "museutils.js", "jquery.watch.js", "detalle-producto.css"], "outOfDate":[]};
</script>
  
  <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
  <meta name="generator" content="2015.0.0.309"/>
  <link rel="shortcut icon" href="/images/favicon.ico?3870392658"/>
  <title>Detalle producto</title>
  <!-- CSS -->
  <link rel="stylesheet" type="text/css" href="/css/site_global.css?4052507572"/>
  <link rel="stylesheet" type="text/css" href="/css/master_a-p_g_-maestra.css?117209650"/>
  <link rel="stylesheet" type="text/css" href="/css/detalle-producto.css?141722931" id="pagesheet"/>
  <!--[if lt IE 9]>
  <link rel="stylesheet" type="text/css" href="css/iefonts_detalle-producto.css?3781601691"/>
  <![endif]-->
  <!-- Other scripts -->
  <script src="/scripts/jquery.zoom.min.js" type="text/javascript"></script>
  <script type="text/javascript">
   document.documentElement.className += ' js';
var __adobewebfontsappname__ = "muse";
</script>
  <!-- JS includes -->
  <script type="text/javascript">
   document.write('\x3Cscript src="' + (document.location.protocol == 'https:' ? 'https:' : 'http:') + '//webfonts.creativecloud.com/lato:n7,i4:default;dosis:n4,n5:default.js" type="text/javascript">\x3C/script>');

</script>

    <style>
        
        /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1000000; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    }
    
    /* Modal Content */
    .modal-content {
        background-color: #111111;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        width: 85%;
        margin-top: -80px;
    }
    
    /* The Close Button */
    .close {
        color: #aaaaaa;
        float: right;
        font-size: 28px;
        font-weight: bold;
    }
    
    .close:hover,
    .close:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
    }
    
    .img_aspect1 {
      display: block;
      max-width:800px;
      max-height:600px;
      min-width:700px;
      min-height:350px;
      width: auto;
      height: auto;
      zoom: 1.15;
      -moz-transform: scale(1.15);
    }
    
    </style>

   </head>
 <body>
     
     
     <!-- The Modal -->
    <div id="myModal" class="modal">
    
      <!-- Modal content -->
      <div class="modal-content">
        <span class="close">&times;</span>
        <!--p>Some text in the Modal..</p-->
        <center><img id="modal_img" class="img_aspect1" width="700" height="700" src=""></center>
      </div>
    
    </div>

  <div class="clearfix" id="page"><!-- column -->
   <div class="position_content" id="page_position_content">
    <div class="browser_width colelem" id="u82-bw">
     <div id="u82"><!-- group -->
      <div class="clearfix" id="u82_align_to_page">
       <a class="nonblock nontext grpelem" id="u143" href="https://www.facebook.com/GRUPO-BARR-1360082537433105/" target="_blank"><!-- simple frame --></a>
       <div class="clip_frame grpelem" id="u83"><!-- image -->
        <img class="block" id="u83_img" src="/images/autos_seminuevos_barr_inicio_03.png" alt="" width="36" height="38"/>
       </div>
       <div class="clearfix grpelem" id="u141-4"><!-- content -->
        <p>56727829</p>
       </div>
       <div class="clearfix grpelem" id="u138-4"><!-- content -->
        <p>ACEPTAMOS TARJETAS DE CRÉDITO Y MENSUALIDADES SIN INTERESES</p>
       </div>
       <div class="clip_frame grpelem" id="u104"><!-- image -->
        <img class="block" id="u104_img" src="/images/autos_seminuevos_barr_inicio_20.png" alt="" width="48" height="34"/>
       </div>
       <div class="clip_frame grpelem" id="u94"><!-- image -->
        <img class="block" id="u94_img" src="/images/autos_seminuevos_barr_inicio_14.png" alt="" width="71" height="31"/>
       </div>
       <div class="clip_frame grpelem" id="u89"><!-- image -->
        <img class="block" id="u89_img" src="/images/autos_seminuevos_barr_inicio_08.png" alt="" width="72" height="28"/>
       </div>
      </div>
     </div>
    </div>
    <div class="clearfix colelem" id="pu99"><!-- group -->
     <div class="clip_frame grpelem" id="u99"><!-- image -->
      <img class="block" id="u99_img" src="/images/autos_seminuevos_barr_inicio_17.png" alt="" width="280" height="73"/>
     </div>
     <a class="nonblock nontext grpelem" id="u113" href="/index"><!-- simple frame --></a>
     <a class="nonblock nontext clearfix grpelem" id="u135-4" href="/contacto"><!-- content --><p>CONTACTO</p></a>
    </div>
    <div class="browser_width colelem" id="u128-bw">
     <div id="u128"><!-- group -->
      <div class="clearfix" id="u128_align_to_page">
       <div class="clearfix grpelem" id="pu188"><!-- column -->

        <!--a class="nonblock nontext colelem" id="u188" href="/products/6" style="background-image: url('/{{$categories[2]->image}}');"></a>
        <a class="nonblock nontext colelem" id="u201" href="/products/5" style="background-image: url('/{{$categories[1]->image}}');"></a>
        <a class="nonblock nontext colelem" id="u214" href="/products/4" style="background-image: url('/{{$categories[0]->image}}');" ></a-->

        <div class="nonblock nontext colelem" style="margin-left: 2px; margin-top: 0px;">
          <div><img id="accidentados_t" src="/images/accidentado.png" width="237px" height="20px"></div>
          <!--h3 style="margin-left: 63px; color: #EEEEEE; font-weight: bold;" > ACCIDENTADOS </h3-->
          <a href="/products/6" onmouseover="hoverRedA()" onmouseout="hoverNormalA()"><img width="237px" height="150px" src="/{{$categories[2]->image}}"> </a>
        </div>

        <div class="nonblock nontext colelem" style="margin-left: 2px; margin-top: 15px;">
          <div><img id="recuperados_t" src="/images/recuperados.png" width="237px" height="20px"></div>
          <!--h3 style="margin-left: 63px; color: #EEEEEE; font-weight: bold;" > RECUPERADOS </h3-->
          <a href="/products/5" onmouseover="hoverRedR()" onmouseout="hoverNormalR()"><img width="237px" height="150px" src="/{{$categories[1]->image}}"> </a>
        </div>

        <div class="nonblock nontext colelem" style="margin-left: 2px; margin-top: 15px;">
          <div><img id="seminuevos_t" src="/images/seminuevos.png" width="237px" height="20px"></div>
          <!--h3 style="margin-left: 63px; color: #EEEEEE; font-weight: bold;" > SEMINUEVOS </h3-->
          <a href="/products/4" onmouseover="hoverRedS()" onmouseout="hoverNormalS()"><img width="237px" height="150px" src="/{{$categories[0]->image}}"> </a>
        </div>

       </div>
       <div class="clearfix grpelem" id="pu884-10"><!-- column -->
        <div class="clearfix colelem" id="u884-10"><!-- content -->
          <p>{{$product->brand}}</p>
          <p>{{$product->name}}</p>
          <p>{{$product->type}}</p>
          <p>{{$product->year}}</p>
        </div>
        <div class="shadow clip_frame colelem" id="u857"><!-- image -->
         <img class="block" id="u857_img" src="/{{$product->image}}" alt=""  />
        </div>
        <div class="clearfix colelem" id="pu866"><!-- group IMAGES THUMBNAIL-->

         <div class="clip_frame grpelem" id="u866" onclick="changepic0()"><!-- image -->
          <img class="block" id="u866_img" src="/{{$product->image}}" alt="" width="109" />
         </div>
         
         <div class="clip_frame grpelem" id="u872" onclick="changepic1()"><!-- image -->
          <img class="block" id="u872_img" src="/{{$product->thumbnail_1}}" alt="" width="109" />
         </div>
         
         <div class="clip_frame grpelem" id="u875" onclick="changepic2()"><!-- image -->
          <img class="block" id="u875_img" src="/{{$product->thumbnail_2}}" alt="" width="109" />
         </div>
         
         <div class="clip_frame grpelem" id="u878" onclick="changepic3()"><!-- image -->
          <img class="block" id="u878_img" src="/{{$product->thumbnail_3}}" alt="" width="109" />
         </div>
         
         <div class="clip_frame grpelem" id="u881" onclick="changepic4()"><!-- image -->
          <img class="block" id="u881_img" src="/{{$product->thumbnail_4}}" alt="" width="109" />
         </div>
         
         <!--div class="clip_frame grpelem" id="u881" onclick="changepic5()">
          <img class="block" id="u881_img" src="/{{$product->thumbnail_5}}" alt="" width="109" />
         </div-->
         
        </div>
       </div>
      </div>
     </div>
    </div>
    <div class="clearfix colelem" id="u1056-4"><!-- content -->
     <p>DETALLES DE PRODUCTO</p>
    </div>
    <div class="clearfix colelem" id="u1099-10"><!-- content -->
      <p>{{$product->brand}}</p>
      <p>{{$product->name}}</p>
      <p>{{$product->type}}</p>
      <p>{{$product->year}}</p>
    </div>

    <div class="clearfix colelem" id="u1057"><!-- group -->
     <div class="clearfix grpelem" id="u1075-4"><!-- content -->
      <p>ESTADO:  {{$product->state}}</p>
     </div>
     <div class="clearfix grpelem" id="u1076-4"><!-- content -->
      <p>SUCURSAL:  {{$product->subsidiary}}</p>
     </div>
     <div class="clearfix grpelem" id="u1077-4"><!-- content -->
      <p>PRECIO:  {{$product->price}}</p>
     </div>
    </div>

    <div class="clearfix colelem" id="u1058"><!-- group -->
     <div class="clearfix grpelem" id="u1078-4"><!-- content -->
      <p>MENSUALIDAD EN TDC BBVA BANCOMER A:</p>
     </div>
     <div class="clearfix grpelem" id="u1079-4"><!-- content -->
      <p>6 MESES</p>
     </div>
     <div class="clearfix grpelem" id="u1080-4"><!-- content -->
      <p>12 MESES</p>
     </div>
    </div>

    <div class="clearfix colelem" id="pu1081-4"><!-- group -->
     <div class="clearfix grpelem" id="u1081-4"><!-- content -->
      <p>MARCA:  {{$product->brand}}</p>  
     </div>
     <div class="clearfix grpelem" id="u1091-4"><!-- content -->
      <p>MODELO:  {{$product->model}}</p>
     </div>
    </div>

    <div class="clearfix colelem" id="pu1082-4"><!-- group -->
     <div class="clearfix grpelem" id="u1082-4"><!-- content -->
      <p>TIPO:  {{$product->type}}</p>
     </div>
     <div class="clearfix grpelem" id="u1089-4"><!-- content -->
      <p>AÑO:  {{$product->year}}</p>
     </div>
    </div>

    <div class="clearfix colelem" id="pu1083-4"><!-- group -->
     <div class="clearfix grpelem" id="u1083-4"><!-- content -->
      <p>MOTOR:  {{$product->engine}}</p>
     </div>
     <div class="clearfix grpelem" id="u1090-4"><!-- content -->
      <p>TRANSMISIÓN:  {{$product->transmition}}</p>
     </div>
    </div>

    <div class="clearfix colelem" id="pu1084-4"><!-- group -->
     <div class="clearfix grpelem" id="u1084-4"><!-- content -->
      <p>COMBUSTIBLE:  {{$product->combustible}}</p>
     </div>
     <div class="clearfix grpelem" id="u1092-4"><!-- content -->
      <p>KMS:  {{$product->kms}}</p>
     </div>
    </div>

    <div class="clearfix colelem" id="pu1085-4"><!-- group -->
     <div class="clearfix grpelem" id="u1085-4"><!-- content -->
      <p>INTERIOR:  {{$product->interior}}</p>
     </div>
     <div class="clearfix grpelem" id="u1093-4"><!-- content -->
      <p>EXTERIOR:  {{$product->exterior}}</p>
     </div>
    </div>

    <div class="clearfix colelem" id="pu1086-4"><!-- group -->
     <div class="clearfix grpelem" id="u1086-4"><!-- content -->
      <p>PUERTAS:  {{$product->doors}}</p>
     </div>
     <div class="clearfix grpelem" id="u1094-4"><!-- content -->
      <p>LLAVES:  {{$product->llaves}}</p>
     </div>
    </div>

    <div class="clearfix colelem" id="pu1087-4"><!-- group -->
     <div class="clearfix grpelem" id="u1087-4"><!-- content -->
      <p>TENENCIA:  {{$product->tenure}}</p>
     </div>
     <div class="clearfix grpelem" id="u1095-4"><!-- content -->
      <p>VERIFICACIÓN:  {{$product->verification}}</p>
     </div>
    </div>

    <div class="clearfix colelem" id="pu1088-4"><!-- group -->
     <div class="clearfix grpelem" id="u1088-4"><!-- content -->
      <p>REFACTURACIÓN:  {{$product->bill}}</p>
     </div>
     <!--div class="clearfix grpelem" id="u1096-4">
      <p>FACTURADO POR: </p>
     </div-->
    </div>

    <div class="clearfix colelem" id="u1097"><!-- group -->
     <div class="clearfix grpelem" id="u1098-4"><!-- content -->
      <p>OBSERVACIONES:  {{$product->observations}}</p>
     </div>
    </div>

    <a class="nonblock nontext rounded-corners clearfix colelem" id="u1104-4" href="/como-comprar-un-auto"><!-- content --><p>Como comprar esta unidad</p></a>
    <div class="verticalspacer"></div>
    <div class="clearfix colelem" id="pu245"><!-- group -->
     <div class="browser_width grpelem" id="u245-bw">
      <div id="u245"><!-- group -->
       <div class="clearfix" id="u245_align_to_page">
        <div class="clearfix grpelem" id="pu263"><!-- column -->
         <div class="clip_frame colelem" id="u263"><!-- image -->
          <img class="block" id="u263_img" src="/images/autos_seminuevos_barr_inicio_59.png" alt="" width="162" height="47"/>
         </div>
         <a class="nonblock nontext clearfix colelem" id="u256-4" href="/index"><!-- content --><p>INICIO</p></a>
        </div>
        <a class="nonblock nontext clearfix grpelem" id="u252-4" href="/products/6"><!-- content --><p>ACCIDENTADOS</p></a>
        <a class="nonblock nontext clearfix grpelem" id="u253-4" href="/products/5"><!-- content --><p>RECUPERADOS</p></a>
        <a class="nonblock nontext clearfix grpelem" id="u254-4" href="/products/4"><!-- content --><p>SEMINUEVOS</p></a>
        <a class="nonblock nontext clearfix grpelem" id="u255-4" href="/contacto"><!-- content --><p>CONTACTO</p></a>
        <a class="nonblock nontext clip_frame grpelem" id="u313" href="https://www.facebook.com/GRUPO-BARR-1360082537433105/" target="_blank"><!-- image --><img class="block" id="u313_img" src="/images/autos_seminuevos_barr_inicio_76.png" alt="" width="15" height="32"/></a>
       </div>
      </div>
     </div>
     <div class="clearfix grpelem" id="u257-4"><!-- content -->
      <p>56727829</p>
     </div>
     <div class="clearfix grpelem" id="u258-4"><!-- content -->
      <p>contacto@autoaccidentado.com.mx</p>
      <p style="margin-top:8px;">clientes@autoaccidentado.com.mx</p>
     </div>
     <div class="clearfix grpelem" id="u260-8"><!-- content -->
      <p>Juan Escutia 199, Col. Independencia</p>
      <p>Delegación Benito Juárez C.P. 03630</p>
      <p>Ciudad de México</p>
     </div>
     <div class="clearfix grpelem" id="u261-6"><!-- content -->
      <p>Lunes a viernes 8:00 a.m a 18:00 p.m</p>
      <p>Sábados 9:00 a.m a 14:00 p.m</p>
     </div>
     <div class="clearfix grpelem" id="u262-4"><!-- content -->
      <p>HORARIO:</p>
     </div>
     <div class="clip_frame grpelem" id="u284"><!-- image -->
      <img class="block" id="u284_img" src="/images/autos_seminuevos_barr_inicio_79.png" alt="" width="19" height="24"/>
     </div>
     <div class="clip_frame grpelem" id="u289"><!-- image -->
      <img class="block" id="u289_img" src="/images/autos_seminuevos_barr_inicio_82.png" alt="" width="23" height="24"/>
     </div>
     <div class="clip_frame grpelem" id="u294"><!-- image -->
      <img class="block" id="u294_img" src="/images/autos_seminuevos_barr_inicio_85.png" alt="" width="25" height="18"/>
     </div>
    </div>
    <div class="clearfix colelem" id="u311-4"><!-- content -->
     <p>Aceptamos tarjetas de crédito</p>
    </div>
    <div class="clearfix colelem" id="pu305"><!-- group -->
     <div class="clip_frame grpelem" id="u305"><!-- image -->
      <img class="block" id="u305_img" src="/images/autos_seminuevos_barr_inicio_14.png" alt="" width="71" height="31"/>
     </div>
     <div class="clip_frame grpelem" id="u274"><!-- image -->
      <img class="block" id="u274_img" src="/images/autos_seminuevos_barr_inicio_65.png" alt="" width="45" height="30"/>
     </div>
     <div class="clip_frame grpelem" id="u269"><!-- image -->
      <img class="block" id="u269_img" src="/images/autos_seminuevos_barr_inicio_62.png" alt="" width="72" height="29"/>
     </div>
    </div>
    <div class="clearfix colelem" id="pu308-4"><!-- group -->
     <div class="clearfix grpelem" id="u308-4"><!-- content -->
      <p>GRUPO BARR 2017. TODOS LOS DERECHOS RESERVADOS</p>
     </div>
     <div class="clearfix grpelem" id="u309-4"><!-- content -->
      <p>AVISO DE PRIVACIDAD</p>
     </div>
     <div class="clearfix grpelem" id="u310-4"><!-- content -->
      <p>DISEÑO POR WOZIAL MARKETING LOVERS</p>
     </div>
    </div>
   </div>
  </div>
  <!-- JS includes -->
  <script type="text/javascript">
   if (document.location.protocol != 'https:') document.write('\x3Cscript src="http://musecdn.businesscatalyst.com/scripts/4.0/jquery-1.8.3.min.js" type="text/javascript">\x3C/script>');
</script>
  <script type="text/javascript">
   window.jQuery || document.write('\x3Cscript src="scripts/jquery-1.8.3.min.js" type="text/javascript">\x3C/script>');
</script>
  
  <script src="/scripts/museutils.js?183364071" type="text/javascript"></script>
  <script src="/scripts/jquery.watch.js?71412426" type="text/javascript"></script>
  <!-- Other scripts -->
  <script type="text/javascript">
   $(document).ready(function() { try {
(function(){var a={},b=function(a){if(a.match(/^rgb/))return a=a.replace(/\s+/g,"").match(/([\d\,]+)/gi)[0].split(","),(parseInt(a[0])<<16)+(parseInt(a[1])<<8)+parseInt(a[2]);if(a.match(/^\#/))return parseInt(a.substr(1),16);return 0};(function(){$('link[type="text/css"]').each(function(){var b=($(this).attr("href")||"").match(/\/?css\/([\w\-]+\.css)\?(\d+)/);b&&b[1]&&b[2]&&(a[b[1]]=b[2])})})();(function(){$("body").append('<div class="version" style="display:none; width:1px; height:1px;"></div>');
for(var c=$(".version"),d=0;d<Muse.assets.required.length;){var f=Muse.assets.required[d],g=f.match(/([\w\-\.]+)\.(\w+)$/),k=g&&g[1]?g[1]:null,g=g&&g[2]?g[2]:null;switch(g.toLowerCase()){case "css":k=k.replace(/\W/gi,"_").replace(/^([^a-z])/gi,"_$1");c.addClass(k);var g=b(c.css("color")),h=b(c.css("background-color"));g!=0||h!=0?(Muse.assets.required.splice(d,1),"undefined"!=typeof a[f]&&(g!=a[f]>>>24||h!=(a[f]&16777215))&&Muse.assets.outOfDate.push(f)):d++;c.removeClass(k);break;case "js":k.match(/^jquery-[\d\.]+/gi)&&
typeof $!="undefined"?Muse.assets.required.splice(d,1):d++;break;default:throw Error("Unsupported file type: "+g);}}c.remove();if(Muse.assets.outOfDate.length||Muse.assets.required.length)c="Puede que determinados archivos falten en el servidor o sean incorrectos. Limpie la cache del navegador e inténtelo de nuevo. Si el problema persiste, póngase en contacto con el administrador del sitio web.",(d=location&&location.search&&location.search.match&&location.search.match(/muse_debug/gi))&&Muse.assets.outOfDate.length&&(c+="\nOut of date: "+Muse.assets.outOfDate.join(",")),d&&Muse.assets.required.length&&(c+="\nMissing: "+Muse.assets.required.join(",")),alert(c)})()})();/* body */
Muse.Utils.transformMarkupToFixBrowserProblemsPreInit();/* body */
Muse.Utils.prepHyperlinks(true);/* body */
Muse.Utils.resizeHeight()/* resize height */
Muse.Utils.fullPage('#page');/* 100% height page */
Muse.Utils.showWidgetsWhenReady();/* body */
Muse.Utils.transformMarkupToFixBrowserProblems();/* body */
} catch(e) { if (e && 'function' == typeof e.notify) e.notify(); else Muse.Assert.fail('Error calling selector function:' + e); }});


    function checkf(str){
      if(str[0] == "/"){
        return str;
      }else{
        return "/" + str;
      }
    }


    var idx = 0;

    var slider = ["{{$product->image}}"];

    if( "{{$product->thumbnail_1}}" != "null" && "{{$product->thumbnail_1}}" != ""){
      slider = slider.concat("{{$product->thumbnail_1}}");
    }
    if( "{{$product->thumbnail_2}}" != "null" && "{{$product->thumbnail_2}}" != ""){
      slider = slider.concat("{{$product->thumbnail_2}}");
    }
    if( "{{$product->thumbnail_3}}" != "null" && "{{$product->thumbnail_3}}" != ""){
      slider = slider.concat("{{$product->thumbnail_3}}");
    }
    if( "{{$product->thumbnail_4}}" != "null" && "{{$product->thumbnail_4}}" != ""){
      slider = slider.concat("{{$product->thumbnail_4}}");
    }
    if( "{{$product->thumbnail_5}}" != "null" && "{{$product->thumbnail_5}}" != "" ){
      slider = slider.concat("{{$product->thumbnail_5}}");
    }


    var array_product = {!! json_encode($product) !!};
    console.log( array_product );
    array_photos = JSON.parse(array_product.photos);
    //console.log( array_photos );
    //console.log( slider );
    var array_photos_all = slider.concat(array_photos);
    console.log( array_photos_all );

    document.getElementById("u866_img").src = checkf(array_photos_all[0]);
    document.getElementById("u872_img").src = checkf(array_photos_all[1]);
    document.getElementById("u875_img").src = checkf(array_photos_all[2]);
    document.getElementById("u878_img").src = checkf(array_photos_all[3]);
    document.getElementById("u881_img").src = checkf(array_photos_all[4]);
    //document.getElementById("u878_img").src = checkf(array_photos_all[5]);

    function changepic0(){
        document.getElementById("u857_img").src = checkf(array_photos_all[0 + idx]);
        document.getElementById("modal_img").src = checkf(array_photos_all[0 + idx]);
        document.getElementById("u866_img").src = checkf(array_photos_all[0 + idx]);
    }

    function changepic1(){
      document.getElementById("u857_img").src = checkf(array_photos_all[1 + idx]);
      document.getElementById("modal_img").src = checkf(array_photos_all[1 + idx]);
      document.getElementById("u872_img").src = checkf(array_photos_all[1 + idx]);
    }

    function changepic2(){
      document.getElementById("u857_img").src = checkf(array_photos_all[2 + idx]);
      document.getElementById("modal_img").src = checkf(array_photos_all[2 + idx]);
      document.getElementById("u875_img").src = checkf(array_photos_all[2 + idx]);
    }

    function changepic3(){
      document.getElementById("u857_img").src = checkf(array_photos_all[3 + idx]);
      document.getElementById("modal_img").src = checkf(array_photos_all[3 + idx]);
      document.getElementById("u878_img").src = checkf(array_photos_all[3 + idx]);
    }

    function changepic4(){
      document.getElementById("u857_img").src = checkf(array_photos_all[4 + idx]);
      document.getElementById("modal_img").src = checkf(array_photos_all[4 + idx]);
      document.getElementById("u881_img").src = checkf(array_photos_all[4 + idx]);
    }

    function changepic5(){
      document.getElementById("u857_img").src = checkf(array_photos_all[5 + idx]);
      document.getElementById("modal_img").src = checkf(array_photos_all[5 + idx]);
      document.getElementById("u866_img").src = checkf(array_photos_all[5 + idx]);
    }



   function hoverRedA(){
    $("#accidentados_t").attr("src", "/images/accidentadoR.png");   
  }
  
  function hoverRedR(){
    $("#recuperados_t").attr("src", "/images/recuperadosR.png");   
  }

  function hoverRedS(){
    $("#seminuevos_t").attr("src", "/images/seminuevosR.png");   
  }


  function hoverNormalA(){
    $("#accidentados_t").attr("src", "/images/accidentado.png");   
  }
  
  function hoverNormalR(){
    $("#recuperados_t").attr("src", "/images/recuperados.png");   
  }

  function hoverNormalS(){
    $("#seminuevos_t").attr("src", "/images/seminuevos.png");   
  }
  

    // Get the modal
    var modal = document.getElementById('myModal');
    
    var btn = document.getElementById("u857");
    
    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];
    
    // When the user clicks the button, open the modal 
      btn.onclick = function() {
          console.log("modal");
          console.log(window.i);
          console.log(window.array);
          //var slider = slider;
          //document.getElementById("modal_img").src = "/"+window.slider[window.i]; 
          modal.style.display = "block";
      }
      /*btn2.onclick = function() {
          modal.style.display = "block";
          console.log("modal");
      }*/

      // When the user clicks on <span> (x), close the modal
      span.onclick = function() {
          modal.style.display = "none";
      }

      // When the user clicks anywhere outside of the modal, close it
      window.onclick = function(event) {
          if (event.target == modal) {
              modal.style.display = "none";
          }
      }
      
      document.getElementById("modal_img").src = slider[0];
      
         
    $("#u99_img").click(function(){
        window.location.replace('/index');
    });
  
</script>
   </body>
</html>
