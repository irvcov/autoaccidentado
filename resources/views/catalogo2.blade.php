<!DOCTYPE html>
<html class="nojs html css_verticalspacer" lang="es-ES">
 <head>

  <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
  <meta name="generator" content="2015.2.0.352"/>
  
  <script type="text/javascript">
   // Update the 'nojs'/'js' class on the html node
document.documentElement.className = document.documentElement.className.replace(/\bnojs\b/g, 'js');

// Check that all required assets are uploaded and up-to-date
if(typeof Muse == "undefined") window.Muse = {}; window.Muse.assets = {"required":[], "outOfDate":[]};
</script>
  
  <link rel="shortcut icon" href="/images/favicon.ico?crc=4217121321"/>
  <title>catalogo</title>
  <!-- CSS -->
  <link rel="stylesheet" type="text/css" href="/css/site_global.css?crc=3916556066"/>
  <link rel="stylesheet" type="text/css" href="/css/master_a-p_g_-maestra.css?crc=11280854"/>
  <link rel="stylesheet" type="text/css" href="/css/catalogo.css?crc=59378480" id="pagesheet"/>
  <!-- IE-only CSS -->
  <!--[if lt IE 9]>
  <link rel="stylesheet" type="text/css" href="css/iefonts_catalogo.css?crc=4238154284"/>
  <![endif]-->
  <!-- Other scripts -->
  <script type="text/javascript">
   var __adobewebfontsappname__ = "muse";
</script>
  <!-- JS includes -->
  <script type="text/javascript">
   document.write('\x3Cscript src="' + (document.location.protocol == 'https:' ? 'https:' : 'http:') + '//webfonts.creativecloud.com/cabin:n4:default;lato:n3,n4:default.js" type="text/javascript">\x3C/script>');
</script>
   </head>
 <body>

  <div class="clearfix" id="page"><!-- column -->
   <div class="position_content" id="page_position_content">
    <div class="clearfix colelem" id="ppu134"><!-- group -->
     <div class="clearfix grpelem" id="pu134"><!-- column -->
      <div class="clip_frame colelem" id="u134"><!-- image -->
       <img class="block" id="u134_img" src="/images/cocinas_industriales_13.png?crc=3812422825" alt="" width="24" height="24"/>
      </div>
      <div class="clip_frame colelem" id="u117"><!-- image -->
       <img class="block" id="u117_img" src="/images/cocinas_industriales_1.png?crc=4081422937" alt="" width="22" height="22"/>
      </div>
     </div>
     <div class="clip_frame grpelem" id="u239"><!-- image -->
      <img class="block" id="u239_img" src="/images/cocinas_industriales_4.png?crc=4293150110" alt="" width="602" height="129"/>
     </div>
     <div class="browser_width grpelem" id="u104-bw">
      <div id="u104"><!-- group -->
       <div class="clearfix" id="u104_align_to_page">
        <div class="clearfix grpelem" id="pu123-4"><!-- group -->
         <div class="clearfix grpelem" id="u123-4"><!-- content -->
          <p>Tel. 3811-7255 / 3633-6326</p>
         </div>
         <div class="clearfix grpelem" id="u125-5"><!-- content -->
          <p><span id="u125">​</span><span class="actAsInlineDiv normal_text" id="u127"><!-- content --><span class="actAsDiv clearfix excludeFromNormalFlow" id="u126-4"><!-- content --><span class="actAsPara">Whatsapp: (33) 3826-6020</span></span></span></p>
         </div>
        </div>
        <div class="clip_frame grpelem" id="u128"><!-- image -->
         <img class="block" id="u128_img" src="/images/cocinas_industriales_13.png?crc=3812422825" alt="" width="36" height="36"/>
        </div>
        <div class="grpelem" id="u2250"><!-- simple frame --></div>
        <div class="grpelem" id="u2246"><!-- simple frame --></div>
       </div>
      </div>
     </div>
     <a class="nonblock nontext clip_frame grpelem" id="u167" href="index.html"><!-- image --><img class="block" id="u167_img" src="/images/cocinas_industriales_5.png?crc=392406310" alt="" width="130" height="32"/></a>
     <a class="nonblock nontext clip_frame grpelem" id="u173" href="nosotros.html"><!-- image --><img class="block" id="u173_img" src="/images/cocinas_industriales_6.png?crc=3956606973" alt="" width="130" height="32"/></a>
     <a class="nonblock nontext MuseLinkActive clip_frame grpelem" id="u179" href="catalogo.html"><!-- image --><img class="block" id="u179_img" src="/images/cocinas_industriales_7.png?crc=4259449217" alt="" width="130" height="32"/></a>
     <a class="nonblock nontext clip_frame grpelem" id="u185" href="promocion.html"><!-- image --><img class="block" id="u185_img" src="/images/cocinas_industriales_8.png?crc=237436024" alt="" width="130" height="32"/></a>
     <a class="nonblock nontext clip_frame grpelem" id="u191" href="pedido.html"><!-- image --><img class="block" id="u191_img" src="/images/cocinas_industriales_9.png?crc=4214725029" alt="" width="130" height="32"/></a>
     <a class="nonblock nontext clip_frame grpelem" id="u197" href="contacto.html"><!-- image --><img class="block" id="u197_img" src="/images/cocinas_industriales_10.png?crc=3929775444" alt="" width="130" height="32"/></a>
     <a class="nonblock nontext clip_frame grpelem" id="u252" href="pedido.html"><!-- image --><img class="block" id="u252_img" src="/images/cocinas_industriales_12.png?crc=198328861" alt="" width="80" height="65"/></a>
     <div class="clearfix grpelem" id="u1323-4"><!-- content -->
       @if($promotion == false)
        <p>EQUIPO COCINA + REFRIGERACIÓN</p>
       @endif
       @if($promotion == true)
        <img class="block" id="" src="/images/imagen_promocion2.png" alt="" width="400" height="45"/>
       @endif
     </div>
     <div class="clip_frame grpelem" id="u3422"><!-- image -->
      <img class="block" id="u3422_img" src="/images/ecylogo.png?crc=212750271" alt="" width="221" height="159"/>
     </div>
    </div>
    <div class="clearfix colelem" id="pu3915"><!-- group -->
     <div class="browser_width grpelem" id="u3915-bw">
      <div class="museBGSize" id="u3915"><!-- simple frame --></div>
     </div>
     <a class="nonblock nontext clip_frame grpelem" id="u3916" href="/catalogo/subcategory/refrigeracion"><!-- image --><img class="block" id="u3916_img" src="/images/cocinas_industriales_210.png?crc=3849748770" alt="" width="115" height="115"/></a>
     <a class="nonblock nontext clip_frame grpelem" id="u3918" href="/catalogo/subcategory/coccion"><!-- image --><img class="block" id="u3918_img" src="/images/cocinas_industriales_34.png?crc=4229509866" alt="" width="115" height="115"/></a>
     <a class="nonblock nontext clip_frame grpelem" id="u3920" href="/catalogo/subcategory/refrigeracion"><!-- image --><img class="block" id="u3920_img" src="/images/cocinas_industriales_44.png?crc=4254332418" alt="" width="115" height="115"/></a>
     <a class="nonblock nontext clip_frame grpelem" id="u3922" href="/catalogo/subcategory/refrigeracion"><!-- image --><img class="block" id="u3922_img" src="/images/cocinas_industriales_54.png?crc=111487609" alt="" width="115" height="115"/></a>
     <a class="nonblock nontext clip_frame grpelem" id="u3924" href="/catalogo/subcategory/refrigeracion"><!-- image --><img class="block" id="u3924_img" src="/images/cocinas_industriales_63.png?crc=3960038689" alt="" width="115" height="115"/></a>
     <a class="nonblock nontext clip_frame grpelem" id="u3926" href="/catalogo/subcategory/refrigeracion"><!-- image --><img class="block" id="u3926_img" src="/images/cocinas_industriales_73.png?crc=525669113" alt="" width="115" height="115"/></a>
    </div>
    <div class="clearfix colelem" id="pppu3940"><!-- group -->

     <div class="clearfix grpelem" id="ppu3940"><!-- column 1-->

    @foreach ($products as $product)

      @if($loop->iteration%3  == 0 )
      <div class="clearfix colelem" id="pu3946"><!-- group 1-->
       <div class="clip_frame grpelem" id="u3946"><!-- image -->
        <img class="block" id="u3946_img" src="/{{$product->image}}" alt="" width="215" height="210"/>
        <img class="block" id="u2341_img" src="/images/cocinas_industriales_232_1.png" alt="" width="215" height="48"/>

       </div>
       <a class="nonblock nontext clip_frame grpelem" id="u3970" href="/detalle/add/{{$product->id}}"><!-- image --><img class="block" id="u3970_img" src="/images/cocinas_industriales_164.png?crc=3892971852" alt="" width="23" height="23"/></a>
       <a class="nonblock nontext clip_frame grpelem" id="u3994" href="pedido"><!-- image --><img class="block" id="u3994_img" src="/images/cocinas_industriales_172.png?crc=211789554" alt="" width="23" height="26"/></a>
       <div class="clearfix grpelem" id="u4041-6"><!-- content -->
        <p>{{substr($product->name,0,10)}} -</p>
        <p{{$product->category_id}} -</p>
        <p>{{$product->brand}}</p>
       </div>
      </div>
      @endif
      
    @endforeach

     </div>



     <div class="clearfix grpelem" id="ppu3956"><!-- column 2-->

      @foreach ($products as $product)

       @if($loop->iteration%3  == 1)
        <div class="clearfix colelem" id="pu3962"><!-- group 2-->
         <div class="clip_frame grpelem" id="u3962"><!-- image -->
          <img class="block" id="u3962_img" src="/{{$product->image}}" alt="" width="215" height="210"/>
          <img class="block" id="u2341_img" src="/images/cocinas_industriales_232_1.png" alt="" width="215" height="48"/>
         
         </div>

         <a class="nonblock nontext clip_frame grpelem" id="u3986" href="/detalle/add/{{$product->id}}"><!-- image --><img class="block" id="u3986_img" src="/images/cocinas_industriales_164.png?crc=3892971852" alt="" width="23" height="23"/></a>
         <a class="nonblock nontext clip_frame grpelem" id="u4010" href="pedido.html"><!-- image --><img class="block" id="u4010_img" src="/images/cocinas_industriales_172.png?crc=211789554" alt="" width="23" height="26"/></a>
         <div class="clearfix grpelem" id="u4049-6"><!-- content -->
          <p>{{substr($product->name,0,10)}} -</p>
          <p{{$product->category_id}} -</p>
          <p>{{$product->brand}}</p>
         </div>
        </div>
        @endif

      @endforeach

     </div>



     <div class="clearfix grpelem" id="ppu3948"><!-- column 3-->



      @foreach ($products as $product)

       @if($loop->iteration%3  == 2)
        <div class="clearfix colelem" id="pu3962"><!-- group 2-->
         <div class="clip_frame grpelem" id="u3962"><!-- image -->
          <img class="block" id="u3962_img" src="/{{$product->image}}" alt="" width="215" height="210"/>
          <img class="block" id="u2341_img" src="/images/cocinas_industriales_232_1.png" alt="" width="215" height="48"/>
         
         </div>

         <a class="nonblock nontext clip_frame grpelem" id="u3986" href="/detalle/add/{{$product->id}}"><!-- image --><img class="block" id="u3986_img" src="/images/cocinas_industriales_164.png?crc=3892971852" alt="" width="23" height="23"/></a>
         <a class="nonblock nontext clip_frame grpelem" id="u4010" href="pedido.html"><!-- image --><img class="block" id="u4010_img" src="/images/cocinas_industriales_172.png?crc=211789554" alt="" width="23" height="26"/></a>
         <div class="clearfix grpelem" id="u4049-6"><!-- content -->
          <p>{{substr($product->name,0,10)}} -</p>
          <p{{$product->category_id}} -</p>
          <p>{{$product->brand}}</p>
         </div>
        </div>
        @endif

      @endforeach
      
      <!--  
      @foreach ($products as $product)

        @if($loop->iteration%3  == 2)
        <div class="clearfix colelem" id="pu3950">
         <div class="clip_frame grpelem" id="u3950">
          <img class="block" id="u3950_img" src="/{{$product->image}}" alt="" width="215" height="210"/>
          <img class="block" id="u2341_img" src="/images/cocinas_industriales_232_1.png" alt="" width="215" height="48"/>

         </div>
         <a class="nonblock nontext clip_frame grpelem" id="u3974" href="/detalle/add/{{$product->id}}"><img class="block" id="u3974_img" src="/images/cocinas_industriales_164.png?crc=3892971852" alt="" width="23" height="23"/></a>
         <a class="nonblock nontext clip_frame grpelem" id="u3998" href="pedido.html"><img class="block" id="u3998_img" src="/images/cocinas_industriales_172.png?crc=211789554" alt="" width="23" height="26"/></a>
         <div class="clearfix grpelem" id="u4043-6">
          <p>{{substr($product->name,0,10)}} -</p>
          <p{{$product->category_id}} -</p>
          <p>{{$product->brand}}</p>
         </div>
        </div>
        @endif

      @endforeach
      -->
     </div>


     <div class="clip_frame grpelem" id="u3913"><!-- image -->
      <img class="block" id="u3913_img" src="/images/cocinas_industriales_82.png?crc=404463976" alt="" width="278" height="1105"/>
     </div>
     <a class="nonblock nontext clip_frame grpelem" id="u3928" href="/catalogo/brand/OJEDA"><img class="block" id="u3928_img" src="/images/cocinas_industriales_92.png?crc=4152931304" alt="" width="140" height="31"/></a>
     <a class="nonblock nontext clip_frame grpelem" id="u3930" href="/catalogo/brand/OJEDA"><img class="block" id="u3930_img" src="/images/cocinas_industriales_102.png?crc=268627093" alt="" width="137" height="28"/></a>
     <a class="nonblock nontext clip_frame grpelem" id="u3932" href="/catalogo/brand/OJEDA"><img class="block" id="u3932_img" src="/images/cocinas_industriales_11.png?crc=3970104843" alt="" width="146" height="33"/></a>
     <a class="nonblock nontext clip_frame grpelem" id="u3934" href="/catalogo/brand/OJEDA"><img class="block" id="u3934_img" src="/images/cocinas_industriales_123.png?crc=28892737" alt="" width="83" height="37"/></a>
     <a class="nonblock nontext clip_frame grpelem" id="u3936" href="/catalogo/brand/CRIOTEC"><img class="block" id="u3936_img" src="/images/cocinas_industriales_133.png?crc=178721467" alt="" width="108" height="34"/></a>
     <a class="nonblock nontext clip_frame grpelem" id="u3938" href="/catalogo/brand/OJEDA"><img class="block" id="u3938_img" src="/images/cocinas_industriales_143.png?crc=4019583141" alt="" width="145" height="30"/></a>
     <a class="nonblock nontext clip_frame grpelem" id="u4012" href="/catalogo/brand/OJEDA"><img class="block" id="u4012_img" src="/images/cocinas_industriales_92.png?crc=4152931304" alt="" width="140" height="31"/></a>
     <!--a class="nonblock nontext clip_frame grpelem" id="u4014" href="http://wozial.com"><img class="block" id="u4014_img" src="/images/cocinas_industriales_102.png?crc=268627093" alt="" width="137" height="28"/></a>
     <a class="nonblock nontext clip_frame grpelem" id="u4016" href="http://wozial.com"><img class="block" id="u4016_img" src="/images/cocinas_industriales_11.png?crc=3970104843" alt="" width="146" height="33"/></a>
     <a class="nonblock nontext clip_frame grpelem" id="u4018" href="http://wozial.com"><img class="block" id="u4018_img" src="/images/cocinas_industriales_123.png?crc=28892737" alt="" width="83" height="37"/></a>
     <a class="nonblock nontext clip_frame grpelem" id="u4020" href="http://wozial.com"><img class="block" id="u4020_img" src="/images/cocinas_industriales_133.png?crc=178721467" alt="" width="108" height="34"/></a>
     <a class="nonblock nontext clip_frame grpelem" id="u4022" href="http://wozial.com"><img class="block" id="u4022_img" src="/images/cocinas_industriales_143.png?crc=4019583141" alt="" width="145" height="30"/></a>
     <a class="nonblock nontext clip_frame grpelem" id="u4024" href="http://wozial.com"><img class="block" id="u4024_img" src="/images/cocinas_industriales_92.png?crc=4152931304" alt="" width="140" height="31"/></a>
     <a class="nonblock nontext clip_frame grpelem" id="u4026" href="http://wozial.com"><img class="block" id="u4026_img" src="/images/cocinas_industriales_102.png?crc=268627093" alt="" width="137" height="28"/></a>
     <a class="nonblock nontext clip_frame grpelem" id="u4028" href="http://wozial.com"><img class="block" id="u4028_img" src="/images/cocinas_industriales_11.png?crc=3970104843" alt="" width="146" height="33"/></a>
     <a class="nonblock nontext clip_frame grpelem" id="u4030" href="http://wozial.com"><img class="block" id="u4030_img" src="/images/cocinas_industriales_123.png?crc=28892737" alt="" width="83" height="37"/></a>
     <a class="nonblock nontext clip_frame grpelem" id="u4032" href="http://wozial.com"><img class="block" id="u4032_img" src="/images/cocinas_industriales_133.png?crc=178721467" alt="" width="108" height="34"/></a>
     <a class="nonblock nontext clip_frame grpelem" id="u4034" href="http://wozial.com"><img class="block" id="u4034_img" src="/images/cocinas_industriales_143.png?crc=4019583141" alt="" width="145" height="30"/></a-->
    </div>


    <div class="verticalspacer" data-offset-top="1554" data-content-above-spacer="1554" data-content-below-spacer="524"></div>
    <div class="clearfix colelem" id="pu3432"><!-- group -->
     <div class="clip_frame grpelem" id="u3432"><!-- image -->
      <img class="block" id="u3432_img" src="/images/ecylogo.png?crc=212750271" alt="" width="221" height="159"/>
     </div>
     <div class="browser_width grpelem" id="u265-bw">
      <div id="u265"><!-- column -->
       <div class="clearfix" id="u265_align_to_page">
        <div class="position_content" id="u265_position_content">
         <a class="nonblock nontext clip_frame colelem" id="u328" href="index.html"><!-- image --><img class="block" id="u328_img" src="/images/cocinas_industriales_23.png?crc=26555268" alt="" width="182" height="30"/></a>
         <a class="nonblock nontext clip_frame colelem" id="u334" href="nosotros.html"><!-- image --><img class="block" id="u334_img" src="/images/cocinas_industriales_24.png?crc=341602865" alt="" width="182" height="30"/></a>
         <a class="nonblock nontext clip_frame colelem" id="u340" href="promocion.html"><!-- image --><img class="block" id="u340_img" src="/images/cocinas_industriales_25.png?crc=15272677" alt="" width="182" height="30"/></a>
         <div class="clip_frame colelem" id="u346"><!-- image -->
          <img class="block" id="u346_img" src="/images/cocinas_industriales_26.png?crc=507103085" alt="" width="182" height="30"/>
         </div>
         <div class="clearfix colelem" id="pu2262"><!-- group -->
          <div class="grpelem" id="u2262"><!-- simple frame --></div>
          <div class="grpelem" id="u2258"><!-- simple frame --></div>
          <div class="grpelem" id="u2254"><!-- simple frame --></div>
          <div class="clearfix grpelem" id="pu352"><!-- column -->
           <a class="nonblock nontext clip_frame colelem" id="u352" href="pedido.html"><!-- image --><img class="block" id="u352_img" src="/images/cocinas_industriales_27.png?crc=4250231700" alt="" width="182" height="30"/></a>
           <a class="nonblock nontext clip_frame colelem" id="u358" href="contacto.html"><!-- image --><img class="block" id="u358_img" src="/images/cocinas_industriales_28.png?crc=264820507" alt="" width="182" height="30"/></a>
          </div>
         </div>
         <div class="clearfix colelem" id="pu282-4"><!-- group -->
          <div class="clearfix grpelem" id="u282-4"><!-- content -->
           <p>(33) 3826-6020</p>
          </div>
          <div class="clip_frame grpelem" id="u289"><!-- image -->
           <img class="block" id="u289_img" src="/images/cocinas_industriales_223.png?crc=4161515262" alt="" width="26" height="26"/>
          </div>
          <div class="clearfix grpelem" id="u274-5"><!-- content -->
           <p>Tel. 3811-7255 <span id="u274-2">/ 3633-6326</span></p>
          </div>
          <div class="clearfix grpelem" id="u272-4"><!-- content -->
           <p>ECY refrigeración de Jalisco 2017 Todos los derechos reservados.</p>
          </div>
         </div>
        </div>
       </div>
      </div>
     </div>
     <div class="clip_frame grpelem" id="u283"><!-- image -->
      <img class="block" id="u283_img" src="/images/cocinas_industriales_212.png?crc=54202197" alt="" width="26" height="26"/>
     </div>
    </div>
    <a class="nonblock nontext clearfix colelem" id="u273-4" href="http://wozial.com"><!-- content --><p>Diseño por Wozial Marketing Lovers.</p></a>
   </div>
  </div>
  <!-- Other scripts -->

<script type="text/javascript">
   if (document.location.protocol != 'https:') document.write('\x3Cscript src="http://musecdn.businesscatalyst.com/scripts/4.0/jquery-1.8.3.min.js" type="text/javascript">\x3C/script>');
</script>
  <script type="text/javascript">
   window.jQuery || document.write('\x3Cscript src="/js/jquery-1.8.3.min.js" type="text/javascript">\x3C/script>');
</script>
  <script src="/js/museutils.js?183364071" type="text/javascript"></script>
  <script src="/js/jquery.musepolyfill.bgsize.js?4004268962" type="text/javascript"></script>
  <script src="/js/jquery.watch.js?71412426" type="text/javascript"></script>
  <script src="/js/webpro.js?3803554875" type="text/javascript"></script>
  <script src="/js/musewpslideshow.js?242596657" type="text/javascript"></script>
  <script src="/js/jquery.museoverlay.js?493285861" type="text/javascript"></script>
  <script src="/js/touchswipe.js?4038331989" type="text/javascript"></script>

  <script type="text/javascript">
   window.Muse.assets.check=function(){if(!window.Muse.assets.checked){window.Muse.assets.checked=!0;var a={},b=function(){$('link[type="text/css"]').each(function(){var b=($(this).attr("href")||"").match(/\/?css\/([\w\-]+\.css)\?crc=(\d+)/);b&&b[1]&&b[2]&&(a[b[1]]=b[2])})},c=function(a){if(a.match(/^rgb/))return a=a.replace(/\s+/g,"").match(/([\d\,]+)/gi)[0].split(","),(parseInt(a[0])<<16)+(parseInt(a[1])<<8)+parseInt(a[2]);if(a.match(/^\#/))return parseInt(a.substr(1),16);return 0},d=function(d){if("undefined"!==
typeof $){b();$("body").append('<div class="version" style="display:none; width:1px; height:1px;"></div>');for(var f=$(".version"),j=0;j<Muse.assets.required.length;){var h=Muse.assets.required[j],i=h.match(/([\w\-\.]+)\.(\w+)$/),k=i&&i[1]?i[1]:null,i=i&&i[2]?i[2]:null;switch(i.toLowerCase()){case "css":k=k.replace(/\W/gi,"_").replace(/^([^a-z])/gi,"_$1");f.addClass(k);var i=c(f.css("color")),l=c(f.css("background-color"));i!=0||l!=0?(Muse.assets.required.splice(j,1),"undefined"!=typeof a[h]&&(i!=
a[h]>>>24||l!=(a[h]&16777215))&&Muse.assets.outOfDate.push(h)):j++;f.removeClass(k);break;case "js":k.match(/^jquery-[\d\.]+/gi)&&typeof $!="undefined"?Muse.assets.required.splice(j,1):j++;break;default:throw Error("Unsupported file type: "+i);}}f.remove()}if(Muse.assets.outOfDate.length||Muse.assets.required.length)f="Some files on the server may be missing or incorrect. Clear browser cache and try again. If the problem persists please contact website author.",d&&Muse.assets.outOfDate.length&&(f+="\nOut of date: "+Muse.assets.outOfDate.join(",")),d&&Muse.assets.required.length&&(f+="\nMissing: "+Muse.assets.required.join(",")),
alert(f)};location&&location.search&&location.search.match&&location.search.match(/muse_debug/gi)?setTimeout(function(){d(!0)},5E3):d()}};
var muse_init=function(){require.config({baseUrl:""});require(["/scripts/museutils.js","/scripts/whatinput.js","/scripts/jquery.watch.js","/scripts/jquery.musepolyfill.bgsize.js"],function(){$(document).ready(function(){try{
window.Muse.assets.check();/* body */
Muse.Utils.transformMarkupToFixBrowserProblemsPreInit();/* body */
Muse.Utils.prepHyperlinks(true);/* body */
Muse.Utils.resizeHeight('.browser_width');/* resize height */
Muse.Utils.requestAnimationFrame(function() { $('body').addClass('initialized'); });/* mark body as initialized */
Muse.Utils.fullPage('#page');/* 100% height page */
Muse.Utils.showWidgetsWhenReady();/* body */
Muse.Utils.transformMarkupToFixBrowserProblems();/* body */
}catch(a){if(a&&"function"==typeof a.notify?a.notify():Muse.Assert.fail("Error calling selector function: "+a),false)throw a;}})})};

</script>
  <!-- RequireJS script -->
  <script src="/scripts/require.js?crc=228336483" type="text/javascript" async data-main="/scripts/museconfig.js?crc=483509463" onload="requirejs.onError = function(requireType, requireModule) { if (requireType && requireType.toString && requireType.toString().indexOf && 0 <= requireType.toString().indexOf('#scripterror')) window.Muse.assets.check(); }" onerror="window.Muse.assets.check();"></script>
   </body>
</html>
