<td>
	<img src="/{{ $entry->{$column['name']} }}"
	@if (isset($column['attributes']))
         @foreach ($column['attributes'] as $attribute => $value)
             @if (is_string($attribute))
             {{ $attribute }}="{{ $value }}"
             @endif
         @endforeach
     @else
         style="width: 48px;"
     @endif
 alt=""> </td>