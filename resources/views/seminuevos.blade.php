<!DOCTYPE html>
<html class="html" lang="es-ES">
 <head>

  <script type="text/javascript">
   if(typeof Muse == "undefined") window.Muse = {}; window.Muse.assets = {"required":["jquery-1.8.3.min.js", "museutils.js", "jquery.watch.js", "seminuevos.css"], "outOfDate":[]};
</script>
  
  <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
  <meta name="generator" content="2015.0.0.309"/>
  <link rel="shortcut icon" href="/images/favicon.ico?3870392658"/>

    @if(count($products) > 0)
  <title>{{$categories[ $products[0]->category_id -4]->name}}</title>
    @else
  <title>Categorias</title>
    @endif
    
  <!-- CSS -->
  <link rel="stylesheet" type="text/css" href="/css/site_global.css?4052507572"/>
  <link rel="stylesheet" type="text/css" href="/css/master_a-p_g_-maestra.css?117209650"/>
  <link rel="stylesheet" type="text/css" href="/css/seminuevos.css?182918884" id="pagesheet"/>
  <!--[if lt IE 9]>
  <link rel="stylesheet" type="text/css" href="css/iefonts_seminuevos.css?4254874390"/>
  <![endif]-->
  <!-- Other scripts -->
  <script type="text/javascript">
   document.documentElement.className += ' js';
var __adobewebfontsappname__ = "muse";
</script>
  <!-- JS includes -->
  <script type="text/javascript">
   document.write('\x3Cscript src="' + (document.location.protocol == 'https:' ? 'https:' : 'http:') + '//webfonts.creativecloud.com/dosis:n4,n5:default.js" type="text/javascript">\x3C/script>');
    
    </script>
   </head>
 <body>

  <div class="clearfix" id="page"><!-- column -->
   <div class="position_content" id="page_position_content">
    <div class="browser_width colelem" id="u82-bw">
     <div id="u82"><!-- group -->
      <div class="clearfix" id="u82_align_to_page">
       <a class="nonblock nontext grpelem" id="u143" href="https://www.facebook.com/GRUPO-BARR-1360082537433105/" target="_blank"><!-- simple frame --></a>
       <div class="clip_frame grpelem" id="u83"><!-- image -->
        <img class="block" id="u83_img" src="/images/autos_seminuevos_barr_inicio_03.png" alt="" width="36" height="38"/>
       </div>
       <div class="clearfix grpelem" id="u141-4"><!-- content -->
        <p>56727829</p>
       </div>
       <div class="clearfix grpelem" id="u138-4"><!-- content -->
        <p>ACEPTAMOS TARJETAS DE CRÉDITO Y MENSUALIDADES SIN INTERESES</p>
       </div>
       <div class="clip_frame grpelem" id="u104"><!-- image -->
        <img class="block" id="u104_img" src="/images/autos_seminuevos_barr_inicio_20.png" alt="" width="48" height="34"/>
       </div>
       <div class="clip_frame grpelem" id="u94"><!-- image -->
        <img class="block" id="u94_img" src="/images/autos_seminuevos_barr_inicio_14.png" alt="" width="71" height="31"/>
       </div>
       <div class="clip_frame grpelem" id="u89"><!-- image -->
        <img class="block" id="u89_img" src="/images/autos_seminuevos_barr_inicio_08.png" alt="" width="72" height="28"/>
       </div>
      </div>
     </div>
    </div>
    <div class="clearfix colelem" id="pu99"><!-- group -->
     <div class="clip_frame grpelem" id="u99"><!-- image -->
      <img class="block" id="u99_img" src="/images/autos_seminuevos_barr_inicio_17.png" alt="" width="280" height="73"/>
     </div>
     <a class="nonblock nontext grpelem" id="u113" href="/index"><!-- simple frame --></a>
     <a class="nonblock nontext clearfix grpelem" id="u135-4" href="/contacto"><!-- content --><p>CONTACTO</p></a>
    </div>
    <div class="browser_width colelem" id="u128-bw">
     <div id="u128"><!-- group -->
      <div class="clearfix" id="u128_align_to_page">
       <div class="clearfix grpelem" id="pu188"><!-- column -->

        <!--a class="nonblock nontext colelem" id="u188" href="/products/6" style="background-image: url('../{{$categories[2]->image}}'); "></a>
        <a class="nonblock nontext colelem" id="u201" href="/products/5" style="background-image: url('../{{$categories[1]->image}}'); "></a>
        <a class="nonblock nontext MuseLinkActive colelem" id="u214" href="/products/4"  style="background-image: url('../{{$categories[0]->image}}'); "></a-->

        <div class="nonblock nontext colelem" style="margin-left: 2px; margin-top: 0px;">
          @if(count($products) > 0)    
              @if( $products[0]->category_id == 6)
              <div><img id="accidentados_t" src="/images/accidentadoR.png" width="237px" height="20px"></div>
              @else
              <div><img id="accidentados_t" src="/images/accidentado.png" width="237px" height="20px"></div>
              @endif
          @else
            <div><img id="accidentados_t" src="/images/accidentado.png" width="237px" height="20px"></div>
          @endif
        
          <!--h3 style="margin-left: 63px; color: #EEEEEE; font-weight: bold;" > ACCIDENTADOS </h3-->
          <a href="/products/6" onmouseover="hoverRedA()" onmouseout="hoverNormalA()"><img width="237px" height="150px" src="../{{$categories[2]->image}}"> </a>
        </div>

        <div class="nonblock nontext colelem" style="margin-left: 2px; margin-top: 15px;">
         @if(count($products) > 0) 
              @if( $products[0]->category_id == 5)
              <div><img id="recuperados_t" src="/images/recuperadosR.png" width="237px" height="20px"></div>
              @else
              <div><img id="recuperados_t" src="/images/recuperados.png" width="237px" height="20px"></div>
              @endif
         @else
            <div><img id="recuperados_t" src="/images/recuperados.png" width="237px" height="20px"></div>
          @endif
          
          <!--h3 style="margin-left: 63px; color: #EEEEEE; font-weight: bold;" > RECUPERADOS </h3-->
          <a href="/products/5" onmouseover="hoverRedR()" onmouseout="hoverNormalR()"><img width="237px" height="150px" src="../{{$categories[1]->image}}"> </a>
        </div>

        <div class="nonblock nontext colelem" style="margin-left: 2px; margin-top: 15px;">
         @if(count($products) > 0) 
              @if( $products[0]->category_id == 4)
              <div><img id="seminuevos_t" src="/images/seminuevosR.png" width="237px" height="20px"></div>
              @else
              <div><img id="seminuevos_t" src="/images/seminuevos.png" width="237px" height="20px"></div>
              @endif
         @else
            <div><img id="seminuevos_t" src="/images/seminuevos.png" width="237px" height="20px"></div>
         @endif
         
          <!--h3 style="margin-left: 63px; color: #EEEEEE; font-weight: bold;" > SEMINUEVOS </h3-->
          <a href="/products/4" onmouseover="hoverRedS()" onmouseout="hoverNormalS()"><img width="237px" height="150px" src="../{{$categories[0]->image}}"> </a>
        </div>
        
       </div>
       <div class="clearfix grpelem" id="ppu767-4"><!-- column -->

       @if(count($products) > 0)
        <div class="clearfix colelem" id="pu767-4"><!-- group -->

         <div class="clearfix grpelem" id="u767-4"><!-- content -->
          <p>{{$categories[ $products[0]->category_id -4]->name}}</p>
         </div>
         <div class="clip_frame grpelem" id="u797"><!-- image -->
          <img class="block" id="u797_img" src="/images/autos_accidentados_barr_productos_03.png" alt="" width="31" height="20"/>
         </div>
         <div class="clearfix grpelem" id="u787-4"><!-- content -->
          <p>{{ $stateproduct[ $products[0]->StateProduct_id-1 ]->name }}</p>
         </div>

        </div>

        <div class="shadow clearfix colelem" id="u737"><!-- group -->
         <div class="clip_frame products-container grpelem" id="u747"><!-- image -->
          <img class="block products-image" id="u747_img" src="/{{$products[0]->image}}" alt="" />
         </div>
        </div>

        <div class="clearfix colelem" id="pu777-10"><!-- group -->

         <div class="clearfix grpelem" id="u777-10"><!-- content -->
          <p>{{$products[0]->brand}}</p>
          <p>{{$products[0]->name}}</p>
          <p>{{$products[0]->type}}</p>
          <p>{{$products[0]->year}}</p>
         </div>
         <a class="nonblock nontext grpelem" id="u813" href="/detalle/add/{{$products[0]->id}}"><!-- simple frame --></a>
        </div>
        @endif

        @if(count($products) > 2)
        <div class="clearfix colelem" id="pu768-4"><!-- group -->
         <div class="clearfix grpelem" id="u768-4"><!-- content -->
          <p>{{$categories[ $products[2]->category_id -4]->name}}</p>
         </div>
         <div class="clip_frame grpelem" id="u799"><!-- image -->
          <img class="block" id="u799_img" src="/images/autos_accidentados_barr_productos_03.png" alt="" width="31" height="20"/>
         </div>
         <div class="clearfix grpelem" id="u788-4"><!-- content -->
          <p>{{ $stateproduct[ $products[2]->StateProduct_id-1 ]->name }} </p>
         </div>
        </div>

        <div class="shadow clearfix colelem" id="u738"><!-- group -->
         <div class="clip_frame products-container grpelem" id="u749"><!-- image -->
          <img class="block products-image" id="u749_img" src="/{{$products[2]->image}}" alt="" />
         </div>
        </div>
        @endif

       </div>

       <div class="clearfix grpelem" id="ppu776-4"><!-- column -->

       @if(count($products) > 1)
        <div class="clearfix colelem" id="pu776-4"><!-- group -->
         <div class="clearfix grpelem" id="u776-4"><!-- content -->
          <p>{{$categories[ $products[1]->category_id -4]->name}}</p>
         </div>
         <div class="clip_frame grpelem" id="u811"><!-- image -->
          <img class="block" id="u811_img" src="/images/autos_accidentados_barr_productos_03.png" alt="" width="31" height="20"/>
         </div>
         <div class="clearfix grpelem" id="u796-4"><!-- content -->
          <p>{{ $stateproduct[ $products[1]->StateProduct_id-1 ]->name}}</p>
         </div>
        </div>
        <div class="shadow clearfix colelem" id="u741"><!-- group -->
         <div class="clip_frame products-container grpelem" id="u755"><!-- image -->
          <img class="block products-image" id="u755_img" src="/{{$products[1]->image}}" alt="" />
         </div>
        </div>

        <div class="clearfix colelem" id="pu781-10"><!-- group -->
         <div class="clearfix grpelem" id="u781-10"><!-- content -->
          <p>{{$products[1]->brand}}</p>
          <p>{{$products[1]->name}}</p>
          <p>{{$products[1]->type}}</p>
          <p>{{$products[1]->year}}</p>
         </div>
         <a class="nonblock nontext grpelem" id="u817" href="/detalle/add/{{$products[1]->id}}"><!-- simple frame --></a>
        </div>
        @endif

        @if(count($products)>3)
        <div class="clearfix colelem" id="pu771-4"><!-- group -->
         <div class="clearfix grpelem" id="u771-4"><!-- content -->
          <p>{{$categories[ $products[3]->category_id -4]->name}}</p>
         </div>
         <div class="clip_frame grpelem" id="u805"><!-- image -->
          <img class="block" id="u805_img" src="/images/autos_accidentados_barr_productos_03.png" alt="" width="31" height="20"/>
         </div>
         <div class="clearfix grpelem" id="u791-4"><!-- content -->
          <p>{{ $stateproduct[ $products[3]->StateProduct_id-1 ]->name}}</p>
         </div>
        </div>
        <div class="shadow clearfix colelem" id="u742"><!-- group -->
         <div class="clip_frame products-container grpelem" id="u757"><!-- image -->
          <img class="block products-image" id="u757_img" src="/{{$products[3]->image}}" alt="" />
         </div>
        </div>
        @endif

       </div>

      </div>
     </div>
    </div>

    <div class="clearfix colelem" id="pu778-10"><!-- group -->

    @if(count($products) > 2)
     <div class="clearfix grpelem" id="u778-10"><!-- content -->
      <p>{{$products[2]->brand}}</p>
      <p>{{$products[2]->name}}</p>
      <p>{{$products[2]->type}}</p>
      <p>{{$products[2]->year}}</p>
     </div>
     <a class="nonblock nontext grpelem" id="u814" href="/detalle/add/{{$products[2]->id}}"><!-- simple frame --></a>
     @endif

     @if(count($products) >3)
     <div class="clearfix grpelem" id="u782-10"><!-- content -->
      <p>{{$products[3]->brand}}</p>
      <p>{{$products[3]->name}}</p>
      <p>{{$products[3]->type}}</p>
      <p>{{$products[3]->year}}</p>
     </div>
     <a class="nonblock nontext grpelem" id="u818" href="/detalle/add/{{$products[3]->id}}"><!-- simple frame --></a>
     @endif

    </div>

    @if(count($products) > 4)
    @for($i = 4; $i < count($products); $i+=3)

    <div class="clearfix colelem" id="pu770-4"><!-- group -->

     @if($i < count($products) )
     <div class="clearfix grpelem" id="u770-4"><!-- content -->
      <p>{{$categories[ $products[$i]->category_id -4]->name}}</p>
     </div>
     <div class="clip_frame grpelem" id="u803"><!-- image -->
      <img class="block" id="u803_img" src="/images/autos_accidentados_barr_productos_03.png" alt="" width="31" height="20"/>
     </div>
     <div class="clearfix grpelem" id="u790-4"><!-- content -->
      <p>{{ $stateproduct[ $products[$i]->StateProduct_id-1 ]->name}}</p>
     </div>
     @endif

     @if($i < count($products)-1 )
     <div class="clearfix grpelem" id="u773-4"><!-- content -->
      <p>{{$categories[ $products[$i+1]->category_id -4]->name}}</p>
     </div>
     <div class="clip_frame grpelem" id="u809"><!-- image -->
      <img class="block" id="u809_img" src="/images/autos_accidentados_barr_productos_03.png" alt="" width="31" height="20"/>
     </div>
     <div class="clearfix grpelem" id="u793-4"><!-- content -->
      <p>{{ $stateproduct[ $products[$i+1]->StateProduct_id-1 ]->name}}</p>
     </div>
     @endif
     
     @if($i < count($products)-2 )
     <div class="clearfix grpelem" id="u775-4"><!-- content -->
      <p>{{$categories[ $products[$i+2]->category_id -4]->name}}</p>
     </div>
     <div class="clip_frame grpelem" id="u809_2"  ><!-- image -->
      <img class="block" id="u809_img" src="/images/autos_accidentados_barr_productos_03.png" alt="" width="31" height="20" />
     </div>
     <div class="clearfix grpelem" id="u795-4"><!-- content -->
      <p>{{$stateproduct[ $products[$i+2]->StateProduct_id-1 ]->name}}</p>
     </div>
     @endif

    </div>
    
    <div class="clearfix colelem" id="pu740"><!-- group -->

     @if($i < count($products) )
     <div class="shadow clearfix grpelem" id="u740"><!-- group -->
      <div class="clip_frame products-container grpelem" id="u753"><!-- image -->
       <img class="block products-image" id="u753_img" src="/{{$products[$i]->image}}" alt="" />
      </div>
     </div>
     @endif

     @if($i < count($products)-1 )
     <div class="shadow clearfix grpelem" id="u744"><!-- group -->
      <div class="clip_frame products-container grpelem" id="u761"><!-- image -->
       <img class="block products-image" id="u761_img" src="/{{$products[$i+1]->image}}" alt="" />
      </div>
     </div>
     @endif

     @if($i < count($products)-2 )
     <div class="shadow clearfix grpelem" id="u746"><!-- group -->
      <div class="clip_frame products-container grpelem" id="u765"><!-- image -->
       <img class="block products-image" id="u765_img" src="/{{$products[$i+2]->image}}" alt="" />
      </div>
     </div>
     @endif

    </div>

    <div class="clearfix colelem" id="pu780-10"><!-- group -->

      @if($i < count($products) )
     <div class="clearfix grpelem" id="u780-10"><!-- content -->
      <p>{{$products[$i]->brand}}</p>
      <p>{{$products[$i]->name}}</p>
      <p>{{$products[$i]->type}}</p>
      <p>{{$products[$i]->year}}</p>
     </div>
     <a class="nonblock nontext grpelem" id="u816" href="/detalle/add/{{$products[$i]->id}}"><!-- simple frame --></a>
     @endif

      @if($i < count($products)-1)
     <div class="clearfix grpelem" id="u784-10"><!-- content -->
      <p>{{$products[$i+1]->brand}}</p>
      <p>{{$products[$i+1]->name}}</p>
      <p>{{$products[$i+1]->type}}</p>
      <p>{{$products[$i+1]->year}}</p>
     </div>
     <a class="nonblock nontext grpelem" id="u820" href="/detalle/add/{{$products[$i+1]->id}}"><!-- simple frame --></a>
     @endif

      @if($i < count($products)-2 )
     <div class="clearfix grpelem" id="u786-10"><!-- content -->
      <p>{{$products[$i+2]->brand}}</p>
      <p>{{$products[$i+2]->name}}</p>
      <p>{{$products[$i+2]->type}}</p>
      <p>{{$products[$i+2]->year}}</p>
     </div>
     <a class="nonblock nontext grpelem" id="u822" href="/detalle/add/{{$products[$i+2]->id}}"><!-- simple frame --></a>
     @endif

    </div>

    @endfor
    @endif

    <div class="verticalspacer"></div>
    <div class="clearfix colelem" id="pu245"><!-- group -->
     <div class="browser_width grpelem" id="u245-bw">
      <div id="u245"><!-- group -->
       <div class="clearfix" id="u245_align_to_page">
        <div class="clearfix grpelem" id="pu263"><!-- column -->
         <div class="clip_frame colelem" id="u263"><!-- image -->
          <img class="block" id="u263_img" src="/images/autos_seminuevos_barr_inicio_59.png" alt="" width="162" height="47"/>
         </div>
         <a class="nonblock nontext clearfix colelem" id="u256-4" href="/index"><!-- content --><p>INICIO</p></a>
        </div>
        <a class="nonblock nontext clearfix grpelem" id="u252-4" href="/products/6"><!-- content --><p>ACCIDENTADOS</p></a>
        <a class="nonblock nontext clearfix grpelem" id="u253-4" href="/products/5"><!-- content --><p>RECUPERADOS</p></a>
        <a class="nonblock nontext MuseLinkActive clearfix grpelem" id="u254-4" href="/products/4"><!-- content --><p>SEMINUEVOS</p></a>
        <a class="nonblock nontext clearfix grpelem" id="u255-4" href="/contacto"><!-- content --><p>CONTACTO</p></a>
        <a class="nonblock nontext clip_frame grpelem" id="u313" href="https://www.facebook.com/GRUPO-BARR-1360082537433105/" target="_blank"><!-- image --><img class="block" id="u313_img" src="/images/autos_seminuevos_barr_inicio_76.png" alt="" width="15" height="32"/></a>
       </div>
      </div>
     </div>
     <div class="clearfix grpelem" id="u257-4"><!-- content -->
      <p>56727829</p>
     </div>
     <div class="clearfix grpelem" id="u258-4"><!-- content -->
       <p>contacto@autoaccidentado.com.mx</p>
      <p style="margin-top:8px;">clientes@autoaccidentado.com.mx</p>
     </div>
     <div class="clearfix grpelem" id="u260-8"><!-- content -->
      <p>Juan Escutia 199, Col. Independencia</p>
      <p>Delegación Benito Juárez C.P. 03630</p>
      <p>Ciudad de México</p>
     </div>
     <div class="clearfix grpelem" id="u261-6"><!-- content -->
      <p>Lunes a viernes 8:00 a.m a 18:00 p.m</p>
      <p>Sábados 9:00 a.m a 14:00 p.m</p>
     </div>
     <div class="clearfix grpelem" id="u262-4"><!-- content -->
      <p>HORARIO:</p>
     </div>
     <div class="clip_frame grpelem" id="u284"><!-- image -->
      <img class="block" id="u284_img" src="/images/autos_seminuevos_barr_inicio_79.png" alt="" width="19" height="24"/>
     </div>
     <div class="clip_frame grpelem" id="u289"><!-- image -->
      <img class="block" id="u289_img" src="/images/autos_seminuevos_barr_inicio_82.png" alt="" width="23" height="24"/>
     </div>
     <div class="clip_frame grpelem" id="u294"><!-- image -->
      <img class="block" id="u294_img" src="/images/autos_seminuevos_barr_inicio_85.png" alt="" width="25" height="18"/>
     </div>
    </div>
    <div class="clearfix colelem" id="u311-4"><!-- content -->
     <p>Aceptamos tarjetas de crédito</p>
    </div>
    <div class="clearfix colelem" id="pu305"><!-- group -->
     <div class="clip_frame grpelem" id="u305"><!-- image -->
      <img class="block" id="u305_img" src="/images/autos_seminuevos_barr_inicio_14.png" alt="" width="71" height="31"/>
     </div>
     <div class="clip_frame grpelem" id="u274"><!-- image -->
      <img class="block" id="u274_img" src="/images/autos_seminuevos_barr_inicio_65.png" alt="" width="45" height="30"/>
     </div>
     <div class="clip_frame grpelem" id="u269"><!-- image -->
      <img class="block" id="u269_img" src="/images/autos_seminuevos_barr_inicio_62.png" alt="" width="72" height="29"/>
     </div>
    </div>
    <div class="clearfix colelem" id="pu308-4"><!-- group -->
     <div class="clearfix grpelem" id="u308-4"><!-- content -->
      <p>GRUPO BARR 2017. TODOS LOS DERECHOS RESERVADOS</p>
     </div>
     <div class="clearfix grpelem" id="u309-4"><!-- content -->
      <p>AVISO DE PRIVACIDAD</p>
     </div>
     <div class="clearfix grpelem" id="u310-4"><!-- content -->
      <p>DISEÑO POR WOZIAL MARKETING LOVERS</p>
     </div>
    </div>
   </div>
  </div>
  <div class="preload_images">
   <img class="preload" src="/images/autos_accidentados_barr_productos_05.png" alt=""/>
  </div>
  <!-- JS includes -->
  <script type="text/javascript">
   if (document.location.protocol != 'https:') document.write('\x3Cscript src="http://musecdn.businesscatalyst.com/scripts/4.0/jquery-1.8.3.min.js" type="text/javascript">\x3C/script>');
</script>
  <script type="text/javascript">
   window.jQuery || document.write('\x3Cscript src="scripts/jquery-1.8.3.min.js" type="text/javascript">\x3C/script>');
</script>
  <script src="/scripts/museutils.js?183364071" type="text/javascript"></script>
  <script src="/scripts/jquery.watch.js?71412426" type="text/javascript"></script>
  <!-- Other scripts -->
  <script type="text/javascript">
   $(document).ready(function() { try {
(function(){var a={},b=function(a){if(a.match(/^rgb/))return a=a.replace(/\s+/g,"").match(/([\d\,]+)/gi)[0].split(","),(parseInt(a[0])<<16)+(parseInt(a[1])<<8)+parseInt(a[2]);if(a.match(/^\#/))return parseInt(a.substr(1),16);return 0};(function(){$('link[type="text/css"]').each(function(){var b=($(this).attr("href")||"").match(/\/?css\/([\w\-]+\.css)\?(\d+)/);b&&b[1]&&b[2]&&(a[b[1]]=b[2])})})();(function(){$("body").append('<div class="version" style="display:none; width:1px; height:1px;"></div>');
for(var c=$(".version"),d=0;d<Muse.assets.required.length;){var f=Muse.assets.required[d],g=f.match(/([\w\-\.]+)\.(\w+)$/),k=g&&g[1]?g[1]:null,g=g&&g[2]?g[2]:null;switch(g.toLowerCase()){case "css":k=k.replace(/\W/gi,"_").replace(/^([^a-z])/gi,"_$1");c.addClass(k);var g=b(c.css("color")),h=b(c.css("background-color"));g!=0||h!=0?(Muse.assets.required.splice(d,1),"undefined"!=typeof a[f]&&(g!=a[f]>>>24||h!=(a[f]&16777215))&&Muse.assets.outOfDate.push(f)):d++;c.removeClass(k);break;case "js":k.match(/^jquery-[\d\.]+/gi)&&
typeof $!="undefined"?Muse.assets.required.splice(d,1):d++;break;default:throw Error("Unsupported file type: "+g);}}c.remove();if(Muse.assets.outOfDate.length||Muse.assets.required.length)c="Puede que determinados archivos falten en el servidor o sean incorrectos. Limpie la cache del navegador e inténtelo de nuevo. Si el problema persiste, póngase en contacto con el administrador del sitio web.",(d=location&&location.search&&location.search.match&&location.search.match(/muse_debug/gi))&&Muse.assets.outOfDate.length&&(c+="\nOut of date: "+Muse.assets.outOfDate.join(",")),d&&Muse.assets.required.length&&(c+="\nMissing: "+Muse.assets.required.join(",")),alert(c)})()})();/* body */
Muse.Utils.transformMarkupToFixBrowserProblemsPreInit();/* body */
Muse.Utils.prepHyperlinks(true);/* body */
Muse.Utils.resizeHeight()/* resize height */
Muse.Utils.fullPage('#page');/* 100% height page */
Muse.Utils.showWidgetsWhenReady();/* body */
Muse.Utils.transformMarkupToFixBrowserProblems();/* body */
} catch(e) { if (e && 'function' == typeof e.notify) e.notify(); else Muse.Assert.fail('Error calling selector function:' + e); }});


  window.products = {!! json_encode($products) !!};
  console.log(window.products);

  

  function hoverRedA(){
    $("#accidentados_t").attr("src", "/images/accidentadoR.png");   
  }
  
  function hoverRedR(){
    $("#recuperados_t").attr("src", "/images/recuperadosR.png");   
  }

  function hoverRedS(){
    $("#seminuevos_t").attr("src", "/images/seminuevosR.png");   
  }


  function hoverNormalA(){
    if(window.products[0]['category_id'] != '6'){
      $("#accidentados_t").attr("src", "/images/accidentado.png");  
    }else{
      $("#accidentados_t").attr("src", "/images/accidentadoR.png"); 
    }
      
  }
  
  function hoverNormalR(){
    if(window.products[0]['category_id'] != '5'){
      $("#recuperados_t").attr("src", "/images/recuperados.png"); 
    }else{
      $("#recuperados_t").attr("src", "/images/recuperadosR.png");   
    }
      
  }

  function hoverNormalS(){
    if(window.products[0]['category_id'] != '4'){
      $("#seminuevos_t").attr("src", "/images/seminuevos.png"); 
    }else{
      $("#seminuevos_t").attr("src", "/images/seminuevosR.png"); 
    }
      
  }

  $("#u99_img").click(function(){
        window.location.replace('/index');
    });

</script>
   </body>
</html>
