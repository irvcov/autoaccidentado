"use strict";

;(function(){
	var cart = {
		init : function(){
			$(".product-add").on("click",this.updateQuantity.bind(this,1));
			$(".product-remove").on("click",this.updateQuantity.bind(this,-1));
		},
		updateQuantity : function(q, e){
			e.preventDefault();
			var $quantityItem = $(e.currentTarget).parents(".product").first().find(".product-quantity");
			var quantity = $quantityItem.data("quantity");
			quantity = parseInt(quantity);
			if(isNaN(quantity) || (quantity <= 1 && q === -1 ) ){
				quantity = 1;
			}else{
				quantity+=q;	
			}
			
			$quantityItem.data("quantity",quantity);
			$quantityItem.text(quantity);
			this.updateLink(e,quantity,$quantityItem.data("id"));
		},
		updateLink : function(e,quantity,id){
			var $updateItem = $(e.currentTarget).parents(".product").first().find(".product-update");
			$updateItem.attr("href","/carrito/add/"+id+"?q="+quantity);
		}
	};

	$(document).on("ready",cart.init.bind(cart));

})();

